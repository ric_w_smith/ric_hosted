Files in this directory are scanned each time it changes.

Upgrade files must be 12 numeric digits, traditionally YYYYMMDDHHMM.ext with ext being php or sql. New files must be of greater numeric value than the previous, application will run files since the last recorded number in order.

Only files with php or sql extension will be run. All others will be ignored.

Code that manages this is in application/models/DbTable/Config.php.
