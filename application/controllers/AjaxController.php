<?php

class AjaxController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function getcustomerAction()
    {
        // get customer from the passed-in form data, creating if needed
        $params = $this->_request->getParams();
        $cust = new SY_Model_Customer();
        $cust->first_name = $params['firstName'];
        $cust->middle_name = $params['middleName'];
        $cust->last_name = $params['lastName'];
        $cust->date_of_birth = SY_Model_Customer::formatDOB($params['dob']);
        $cust->drivers_license_number = $params['license'];
        $cust->address_1 = $params['address_1'];
        $cust->address_2 = $params['address_2'];
        $cust->city = $params['city'];
        $cust->state = $params['state'];
        $cust->zip_code = $params['zip'];
        $found = $cust->findOne();
        if(false == $found)
        {
            $cust->save();
            echo json_encode($cust);
        }
        else
        {
            echo json_encode($found);
        }
    }
    
    public function suspenduserAction()
    {
        $user = $this->_request->getParam('id');
        $us = new SY_Model_Login();
        $us->id = $user;
        $use = $us->findOne();
        $use->suspended = $use->suspended ? 0 : 1;
        $use->save();
        echo $use->toJSON();
    }
    
    public function deleteuserAction()
    {
        $user = $this->_request->getParam('id');
        $us = new SY_Model_Login();
        $us->id = $user;
        $use = $us->findOne();
        $use->delete();
        echo $use->toJSON();
    }
    
    public function searchtransactionsAction()
    {
        $customerString = $this->_request->getParam('customer');
        if($customerString == '')
        {
            echo '';
        }
        else
        {
        $cust = new SY_Model_Customer();
        $cust->last_name = $customerString;
        $custs = $cust->findLike();
        $output = array();
        foreach($custs as $thisCust)
        {
            $output[] = $thisCust->toJSON();
        }
        echo json_encode($output);
        }
    }
    
    public function takepictureAction()
    {
        $str = file_get_contents("php://input");
        file_put_contents("/tmp/upload.jpg", pack("H*", $str));
    }
    
    public function scanlicenseAction()
    {
        $license = $this->_request->getParam('license');
        $licObj = new SY_Model_DLicense($license);
        echo json_encode($licObj);
    }
    
    public function createticketAction()
    {
        // scale id
        $sess = new Zend_Session_Namespace('scrapyard');
	$params = array();
	$params['corporate_ticket'] = $this->_request->getParam('corporate_ticket', 0);
        $params['customer_id'] = $this->_request->getParam('customer_id',0);
        $params['scale_id'] = $sess->scaleID;
        //$params['date_created'] = date('Y-m-d H:i:s');
        $params['ticket_state'] = 0;
        $params['login_id'] = $this->_request->getParam('login_id', 0);
		$params['tag_state'] = $this->_request->getParam('tag_state', '');
        $params['tag'] = $this->_request->getParam('tag', '');
        $params['epaform'] = $this->_request->getParam('epaform', '');
        $params['visittype'] = $this->_request->getParam('visittype', '');
        // $params['visittype'] = $this->_request->getParam('visittype','');
        $params['sig_string'] = $this->_request->getParam('sig_string','');
	$params['TicketComments'] = $this->_request->getParam('comments','');
	$params['upload_status'] = 'Pending';
	$params['tag'] = $this->_request->getParam('tag','');
	$ticket = new SY_Model_Ticket($params);
	//$this->_helper->FlashMessenger->addMessage(array('message' => "validate: " . $ticket->validate($params), 'status' => 'error'));
        $ticket->save();
        $ticketId = $ticket->ticketNumber();
        unset($params);
        $params = $this->_request->getParams();
        // now make an entry for each metal brought in
        $metal = new SY_Model_Metal();
        $allMetals = $metal->findAll();
        for($j = 1; $j <= $allMetals[count($allMetals)-1]->id; $j++)
        {
            if (!isset($params['gross'.$j])) {
                $params['gross'.$j] = 0;
            }
            if (!isset($params['tare'.$j])) {
                $params['tare'.$j] = 0;
            }
            if($params['gross'.$j] > 0 && $params['gross'.$j] > $params['tare'.$j])
            {
                $data['ticket_id'] = $ticket->id;
                $data['metal_id'] = $j;
                $data['gross'] = $params['gross'.$j];
                $data['tare'] = $params['tare'.$j];
                $data['price_per_unit'] = isset($params['ppu'.$j]) ? $params['ppu'.$j] : $params['ppuorig'.$j];
                $purchase = new SY_Model_Purchase($data);
                $purchase->save();
                unset($purchase);
            }
        }
        echo $ticketId;
    }
}

