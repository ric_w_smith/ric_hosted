<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    //login page
    public function indexAction()
    {
        if ($this->view->level() == 'office') {
            $this->_redirect('/office/client/search');
        } elseif ($this->view->level() == 'yard') {
            $this->_redirect('/yard/client/search');
        } elseif ($this->view->level() == 'admin') {
            $this->_redirect('/admin/report/basic');
        }
    }

    //starting hub page
    public function mainAction()
    {
        //check creds, if pass set session var, else boot back to index w/message
    }


}

