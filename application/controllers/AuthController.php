<?php

class AuthController extends Zend_Controller_Action
{

    public function init() {
        // Nothing in the auth controller should need layout.
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        
    }
    
    public function processAction() {
        $auth = Zend_Auth::getInstance();
        if ($this->_request->getParam('username') && $this->_request->getParam('password')) {
            $authAdapter = new SY_Model_Auth($this->_request->getParam('username'), $this->_request->getParam('password'));
            $result = $auth->authenticate($authAdapter);
            if (!$result->isValid()) {
                $flash = $this->_helper->getHelper('flashMessenger');
                if ($result->getCode() == Zend_Auth_Result::FAILURE) {
                    $this->_helper->flashMessenger->addMessage(array('status' => 'error', 'message' => "Your account has been suspended."));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('status' => 'error', 'message' => "Invalid username and password."));
                }
                $this->_redirect($this->view->url(array('controller' => 'auth', 'action' => 'index')));
            } else {
                // user/password found, now see if the password is still good
                $login = new SY_Model_Login();
                $login->username = $this->_request->getParam('username');
                $login->password = $this->_request->getParam('password');
                $thisLogin = $login->findOne();
                if($thisLogin->resetpass)
                {
                    // password has been reset
                    $this->_redirect('/auth/resetpass/id/' . $thisLogin->id);
                }
                $this->_helper->flashMessenger->addMessage(array('status' => 'success', 'message' => "Welcome!"));
                $sess = new Zend_Session_Namespace('scrapyard');
                $sess->scaleID = $this->_request->getParam('scale'); // set the scale in the session
                // Login succeeded, send them to the dashboard.
                $this->_redirect($this->view->url(array('controller' => 'index', 'action' => 'index')));
            }
        } else {
            // Invalid Params, send them to index.
            $this->_redirect($this->view->url(array('controller' => 'auth', 'action' => 'index')));
        }
    }
    
    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->flashMessenger->addMessage(array('status' => 'info', 'message' => "You have successfully logged out."));
        $this->_redirect($this->view->url(array('controller' => 'auth', 'action' => 'index'))); // back to login page
    }
}

