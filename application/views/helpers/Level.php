<?php
// require_once(APPLICATION_PATH.'/modules/doctor/models/AuthIdentity.php');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WhoAmI
 *
 * @author wlightning
 */
class Zend_View_Helper_Level extends Zend_View_Helper_Abstract {
    public function level() {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            return $auth->getIdentity()->level;
        } else {
            return 'yard';
        }
    }
}

?>
