<?php

/**
 * Description of SY_Model_Login
 *
 * @author ostin
 */
class SY_Model_Login extends SY_Model_Abstract {
    public $id, $username, $password, $resetpass, $name, $suspended, $level;
    
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Logins();
    }

    public function beforeInsert(&$data)
    {
        
    }
    
    public function beforeUpdate(&$data)
    {
        
    }
    
    public function findAll()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && $varName !== 'defaultPassword' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $c = get_class($this);
            $new = new $c();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }    
    public function validate(&$obj = null)
    {
        if(!is_null($obj) && is_array($obj))
        {
            foreach($obj as $key => $val)
            {
                if(!in_array($key, array_keys(get_class_vars(get_class($this)))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public function __call($name, $arguments)
    {
        if('password' == $name && isset($arguments[0]) && '' != $arguments[0])
        {
            $db = $this->_db->getAdapter();
            $value = $db->quote(crypt($arguments[0]));
            $this->password = $value;
        }
    }
}

?>
