<?php

class SY_Model_Auth implements Zend_Auth_Adapter_Interface {

    /**
     * Stores the last authentication credentials
     * 
     * @var array
     */
    private $lastCreds;

    /**
     * Sets username and password for authentication
     *
     * @return void
     */
    public function __construct($username, $password) {
        $this->lastCreds = array();
        $this->lastCreds['user'] = $username;
        $this->lastCreds['pass'] = $password;
    }

    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot
     *                                     be performed
     * @return Zend_Auth_Result
     */
    public function authenticate() {
        $lookup = new SY_Model_AuthLookup();
        $user = $this->lastCreds['user'];
        $pass = $this->lastCreds['pass'];

        $login = $lookup->lookupUser($user, $pass);
        if ($login) {
            if (is_array($login)) {
                if ($login['suspended'] == '1') {
                    return new Zend_Auth_Result(Zend_Auth_Result::FAILURE);
                }
                $ident = new SY_Model_AuthIdentity($login['id'], $login['username'], $login['name'], $login['level']);
                // session it
                $sess = new Zend_Session_Namespace('scrapyard_auth');
                $sess->user = $ident;
                return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $ident);
            } else {
                return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID);
            }
        } else {
            return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND);
        }
    }
}

