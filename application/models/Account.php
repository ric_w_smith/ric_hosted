<?php

/**
 * Description of SY_Model_Account
 *
 * @author ostin
 */
class SY_Model_Account extends SY_Model_Abstract {
    public $id, $date, $balance;
    
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Accounts();
    }

    public function beforeInsert(&$data)
    {
        $data['date'] = date('Y-m-d');
    }
    
    public function beforeUpdate(&$data)
    {
        $data['date'] = date('Y-m-d');
    }
    
    
    public function validate(&$obj = null)
    {
        if(!is_null($obj) && is_array($obj))
        {
            foreach($obj as $key => $val)
            {
                if(!in_array($key, array_keys(get_class_vars(get_class($this)))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public function save()
    {
        // if we're making a new one, make sure there's not already
        // one with the current date
        //if(!(isset($this->id) && is_numeric($this->id) && 0 != $this->id))
        //{
            //$ac = new SY_Model_Account();
            //$ac->date = date('Y-m-d');
            //$acs = $ac->findAll();
            //if(count($acs) > 0)
            //{
                // already made, return
                //return;
           // }
       // }
        $data = array();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                $data[$varName] = $this->$varName;
            }
            unset($data['_db']);
                        
            if(isset($this->id) && is_numeric($this->id) && 0 != $this->id)
            {
                //$this->beforeUpdate($data);
                $this->_db->update($data, 'id = ' . $this->id);
            }
            else
            {
                //$this->beforeInsert($data);
                $this->_db->insert($data);
                $this->id = $this->_db->getAdapter()->lastInsertId();
            }
    }
    
}

?>
