<?php

/**
 * Description of SY_Model_Scale
 *
 * @author ostin
 */
class SY_Model_Scale extends SY_Model_Abstract {
    public $id, $name;
    
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Scales();
    }

    public function beforeInsert(&$data)
    {
        
    }
    
    public function beforeUpdate(&$data)
    {
        
    }
    
    
    public function validate(&$obj = null)
    {
        if(!is_null($obj) && is_array($obj))
        {
            foreach($obj as $key => $val)
            {
                if(!in_array($key, array_keys(get_class_vars(get_class($this)))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}

?>
