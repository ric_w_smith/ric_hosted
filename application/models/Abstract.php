<?php

/**
 * Description of SY_Model_Abstract
 *
 * @author ostin
 */
abstract class SY_Model_Abstract {
    protected $_db;
    
    public function __construct($object = null)
    {
        if($this->validate($object))
        {
            $obj = (object) $object;
            foreach(get_class_vars(get_class($this)) as $varName => $v)
            {
                if(isset($obj->$varName))
                $this->$varName = $obj->$varName;
            }
        }
    }
    
    abstract public function validate(&$obj = null);
    
    public function load($row)
    {
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db')
                $this->$varName = $row->$varName;
            }
    }
    
    public function findAll()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $c = get_class($this);
            $new = new $c();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }
    
    public function findLike()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && !empty($this->$varName))
                {
                    $se = new Zend_Db_Expr("$varName LIKE '%{$this->$varName}%'");
                    $select->where($se);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $c = get_class($this);
            $new = new $c();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }
    
    public function findOne()
    {
        $check = $this->findAll();
        if(isset($check[0]) && get_class($check[0]) == get_class($this)) return $check[0];
        return false;
    }

    abstract public function beforeInsert(&$data);
    
    abstract public function beforeUpdate(&$data);
    
    public function save()
    {
        $data = array();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                $data[$varName] = $this->$varName;
            }
            unset($data['_db']);
            if(isset($this->id) && is_numeric($this->id) && 0 != $this->id)
            {
                $this->beforeUpdate($data);
                $this->_db->update($data, 'id = ' . $this->id);
            }
            else
            {
                $this->beforeInsert($data);
                $this->_db->insert($data);
                $this->id = $this->_db->getAdapter()->lastInsertId();
            }
    }
    
    public function delete()
    {
        // delete this object
        $useAnd = false;
        $where = '';
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && !empty($this->$varName))
                {
                    if($useAnd) $where .= ' AND ';
                    $where .= "$varName = '{$this->$varName}'";
                    $useAnd = true;
                }
            }
        $this->_db->delete($where);
    }
    
    public function __toString()
    {
        $output = new stdClass();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
        {
            if($varName !== '_db')
            {
                $output->$varName = $this->$varName;
            }
        }
        return serialize($output);
    }
    
    public function toJSON()
    {
        $output = new stdClass();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
        {
            if($varName !== '_db')
            {
                $output->$varName = $this->$varName;
            }
        }
        return json_encode($output);
    }
    
}

?>
