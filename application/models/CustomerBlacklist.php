<?php

/**
 * Description of SY_Model_Customer
 *
 * @author ostin
 */
class SY_Model_Customer {
    protected $_db;
    public $scanned = false;
    public $state, $account_state, $date_created, $date_modified, $picture, $picture_mimetype;
    public $city, $last_name, $first_name, $middle_name;
    public $address_1, $address_2, $drivers_license_number, $date_of_birth, $zip_code;
    
    public function __construct($object = null)
    {
        if(!is_null($object) && get_class($object) == 'SY_Model_DLicense')
        {
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                $this->$varName = $object->$varName;
            }
        }
        $this->_db = new SY_Model_DbTable_Customers();
    }
    
    
    public function load($row)
    {
        
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && $varName !== 'scanned')
                $this->$varName = $row->$varName;
            }
    }
    
    public function findAll()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== 'scanned' && $varName !== '_db' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $new = new SY_Model_Customer();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }
}

?>
