<?php

/*  Notes 
 
	C:\wamp\bin\php\php5.4.12\php.ini removed # (uncomment) line below and restarted servers
	#extension=php_soap.dll
	
*/
class WebserviceClient 
{

    private $webSecurityToken = "";
    private $webServID, $webServPW, $facilityRegNumber, $options;
	protected $_logger, $_sess;
	
	public function __construct()  
	{  
//		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
//    	$options = $bootstrap->getOptions(APPLICATION_ENV);
		$this->_sess = new Zend_Session_Namespace('scrapyard');
		$this->_logger = new ScrapYardLogger();
	}	
		
	public function get_webSecurityToken()
{
		if ( isset($webSecurityToken) ) {
		} else {
			$webSecurityToken = $this->callAuthenticateWS();	
		} 
		return $webSecurityToken;
	}
	
	/* Needs to return security token */
    private function callAuthenticateWS()
	{
		//show all the errors!
		//error_reporting(E_ALL);
		//ini_set('display_errors', '1');
		ini_set('soap.wsdl_cache_enabled', false); //disable caching

		$url = 'https://ext.dps.state.oh.us/ScrapDealerTranService/ScrapDealerWebService.asmx?wsdl';

		$xml_request_a = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> ' .
						 '<soapenv:Header/> <soapenv:Body> <tem:Authenticate> <tem:userName>';
		$xml_request_b = '</tem:userName> <tem:password>';
		$xml_request_c = '</tem:password> </tem:Authenticate> </soapenv:Body> </soapenv:Envelope>';
    
		$xml_request = $xml_request_a.$this->_sess->webServID.$xml_request_b.$this->_sess->webServPW.$xml_request_c;		
// $this->_logger -> postIt('Request XML ' .$xml_request,Zend_Log::ERR); 		
	
		$headers = array( 
			'Content-Type: text/xml; charset="utf-8"', 
			'Content-Length: '.strlen($xml_request), 
			'Accept: text/xml', 
			'Cache-Control: no-cache', 
			'Pragma: no-cache', 
		);
							
		$tuCurl = curl_init(); 
		curl_setopt($tuCurl, CURLOPT_URL, $url); 
        curl_setopt($tuCurl, CURLOPT_PORT , 443); 
        curl_setopt($tuCurl, CURLOPT_VERBOSE, 0); 
        curl_setopt($tuCurl, CURLOPT_HEADER, 0); 
        curl_setopt($tuCurl, CURLOPT_SSLVERSION, 3); 
        curl_setopt($tuCurl, CURLOPT_POST, 1); 
        curl_setopt($tuCurl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($tuCurl, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt($tuCurl, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $xml_request); 
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $headers); 
		$response = curl_exec($tuCurl); 
		if(curl_errno($tuCurl)) { 
			$this->_logger -> postIt('Curl error: ' . curl_error($tuCurl),Zend_Log::ERR); 
			return  "101";
        } 		
//			$info = curl_getinfo($tuCurl); 
//			$this->_logger->postIt('Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'],Zend_Log::ERR); 	
	
		$xml = simplexml_load_string( $response );
		$AuthenicateToken = "";
		foreach($xml->xpath('//soap:Body') as $header) {
			$AuthenicateToken = (string)$header->AuthenticateResponse->AuthenticateResult;
$this->_logger->postIt('This is what the site is returning: '.$AuthenicateToken,Zend_Log::ERR);
		}	
		if ( empty($AuthenicateToken) )  $AuthenicateToken  = "101";
		 
		return $AuthenicateToken;
	}

    public function uploadTickets( $tickets )
	{
		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$options = $bootstrap->getOptions(APPLICATION_ENV);
		$this->getAdminInformation();
		$webServID = $this->_sess->webServID;
		$webServPW = $this->_sess->webServPW;
		$processed = 0;
		$passed = 0;
		$failed = 0;
		$retry  = 0;
		$TransactionResult = "";
		$numberSelected = count($tickets);
		$webEndPoint = $options['webservice'][$this->_sess->webState];
		$driverslicensePath = $options['driverLicensePath'];
		
		// using this code to step through calls so if on is returned for expired auth token we can not increment, call (get_webSecurityToken()) then reprocess it			
		$i = 0;
//$this->_logger->postIt("uploadTickets was passed: ".$numberSelected." tickets",Zend_Log::ERR);
		while ( $i < $numberSelected) {
			$processed += 1;
			$id = $tickets[$i];
$this->_logger->postIt('Processing Ticket id: '.$id, Zend_Log::INFO);	
            $ticketResults = $this->getTicketInfo($id);
			$SecurityToken = $this->get_webSecurityToken();
			if ( empty($SecurityToken) or ( strlen($SecurityToken) == 3 and $SecurityToken == "101" ) ) {
				$failed += 1;
				$this->updateTicketStatus($id,'101', "", "");
			    break;
			} 
/*
Yes, while we are working on a release to fix for this issue, we suggest a workaround of passing empty strings to following fields  
middleName,   suffix,    add2,  containerDesc,   metalArticlesNotRecyclableDesc.   */		
			$xml_request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
			$xml_request = $xml_request .'<soapenv:Header/>';
			$xml_request = $xml_request .'<soapenv:Body>';
			$xml_request = $xml_request .'<tem:ScrapDealerTransaction><tem:authToken>';
			$xml_request = $xml_request . $SecurityToken; 
			$xml_request = $xml_request .'</tem:authToken><tem:facilityRegNumber>';
			$xml_request = $xml_request . $this->_sess->facilityRegNumber;
			$xml_request = $xml_request .'</tem:facilityRegNumber><tem:txnNumber>';			
			$xml_request = $xml_request .$id;
			$xml_request = $xml_request .'</tem:txnNumber><tem:firstName>';
			$xml_request = $xml_request .$ticketResults -> first_name;
			$xml_request = $xml_request .'</tem:firstName><tem:middleName>';
//			$xml_request = $xml_request .$ticketResults -> middle_name;
			$xml_request = $xml_request .'</tem:middleName><tem:lastName>';
 			$xml_request = $xml_request .$ticketResults -> last_name;
			$xml_request = $xml_request .'</tem:lastName><tem:suffix>';
//	        $xml_request = $xml_request .$ticketResults -> ;  
			$xml_request = $xml_request .'</tem:suffix><tem:add1>';
			$xml_request = $xml_request .$ticketResults -> address_1;
			$xml_request = $xml_request .'</tem:add1><tem:add2>';
//			$xml_request = $xml_request .$ticketResults -> address_2;
			$xml_request = $xml_request .'</tem:add2><tem:city>';
			$xml_request = $xml_request .$ticketResults -> city;
			$xml_request = $xml_request .'</tem:city><tem:state>';
			$xml_request = $xml_request .$ticketResults -> state;
			$xml_request = $xml_request .'</tem:state><tem:zip>';
			$xml_request = $xml_request .$ticketResults -> zip_code;
			$xml_request = $xml_request .'</tem:zip><tem:txnDateTime>';
			$xml_request = $xml_request .$ticketResults -> fmtDate;
			$xml_request = $xml_request .'</tem:txnDateTime><tem:idCardImage>';
// eventually the filename will have to be stored, returned by the 	query and replace SpongeBob;
            $xml_request = $xml_request .base64_encode(file_get_contents( $driverslicensePath."SpongeBob.jpg"));  
//			$xml_request = $xml_request .base64_encode($ticketResults -> picture);
			$xml_request = $xml_request .'</tem:idCardImage><tem:photoOfSeller>';
			$xml_request = $xml_request .base64_encode($ticketResults -> picture);
			$xml_request = $xml_request .'</tem:photoOfSeller><tem:containerDesc>';
			$xml_request = $xml_request .'</tem:containerDesc><tem:numberOfContainers>';
			$xml_request = $xml_request .'</tem:numberOfContainers><tem:containerPhoptos><tem:base64Binary>';
			$xml_request = $xml_request .'</tem:base64Binary></tem:containerPhoptos><tem:weightOfArticles>';
			$xml_request = $xml_request .$this->calulateWeight($id);
			$xml_request = $xml_request .'</tem:weightOfArticles><tem:licencePlateNumber>';
			$xml_request = $xml_request .$ticketResults -> tag;
			$xml_request = $xml_request .'</tem:licencePlateNumber><tem:licensePlateIssueState>';
			$xml_request = $xml_request .$ticketResults -> tag_state;
			$xml_request = $xml_request .'</tem:licensePlateIssueState><tem:metalArticlesNotRecyclableDesc>';
			$xml_request = $xml_request .'</tem:metalArticlesNotRecyclableDesc><tem:recycMaterilasNotSpecialPurchaseArticles>';
			$xml_request = $xml_request .$this->getMaterialCodes($id, '0'); 
			$xml_request = $xml_request .'</tem:recycMaterilasNotSpecialPurchaseArticles><tem:recycMaterialsSpecialPurchaseArticles>';
			$xml_request = $xml_request .'</tem:recycMaterialsSpecialPurchaseArticles><tem:recycMaterialsSpecialPurchaseArticlePhotos><tem:base64Binary>';			                                                                                               
			$xml_request = $xml_request .'</tem:base64Binary></tem:recycMaterialsSpecialPurchaseArticlePhotos></tem:ScrapDealerTransaction>';
			$xml_request = $xml_request .'</soapenv:Body>';
			$xml_request = $xml_request .'</soapenv:Envelope>';
 
$this->_logger->postIt('Ticket Request XML: ' .$xml_request, Zend_Log::INFO);	
	//show all the errors!
			//error_reporting(E_ALL);
			//ini_set('display_errors', '1');
			ini_set('soap.wsdl_cache_enabled', false); //disable caching
//			$url = 'https://ext.dps.state.oh.us/ScrapDealerTranService/ScrapDealerWebService.asmx?wsdl';

			$headers = array( 
				'Content-Type: text/xml; charset="utf-8"', 
				'Content-Length: '.strlen($xml_request), 
				'Accept: text/xml', 
				'Cache-Control: no-cache', 
				'Pragma: no-cache', 
			);
			try {							
				$tuCurl = curl_init(); 
				curl_setopt($tuCurl, CURLOPT_URL, $webEndPoint); 
				curl_setopt($tuCurl, CURLOPT_PORT , 443); 
				curl_setopt($tuCurl, CURLOPT_VERBOSE, 0); 
				curl_setopt($tuCurl, CURLOPT_HEADER, 0); 
				curl_setopt($tuCurl, CURLOPT_SSLVERSION, 3); 
				curl_setopt($tuCurl, CURLOPT_POST, 1); 
				curl_setopt($tuCurl, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($tuCurl, CURLOPT_SSL_VERIFYHOST, 0); 
				curl_setopt($tuCurl, CURLOPT_SSL_VERIFYPEER, 0); 
				curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 
				curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $xml_request); 
				curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $headers); 
				$response = curl_exec($tuCurl); 
//$this->_logger->postIt('This is what the site is returning: '.$response,Zend_Log::ERR);		
				if(curl_errno($tuCurl)) { 
					$this->_logger->postIt('Curl error: ' . curl_error($tuCurl),Zend_Log::ERR); 
//				return  "101";
				} 		
//				$info = curl_getinfo($tuCurl); 
//				$this->_logger->postIt('Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'],Zend_Log::ERR); 		

				$xml = simplexml_load_string( $response );
$this->_logger->postIt('This is what the site is returning: '.$response,Zend_Log::ERR);
				$TransactionResult = "";
				foreach($xml->xpath('//soap:Body') as $header) {
					$TransactionResult = (string)$header->ScrapDealerTransactionResponse->ScrapDealerTransactionResult;
				}				
$this->_logger->postIt('TransactionResult: <'.$TransactionResult.'>',Zend_Log::ERR);
				if(strlen($TransactionResult) > 1){ 
					if($TransactionResult == 1)  
						$passed += 1;
					 else 	
						$failed+= 1;							
					$this->updateTicketStatus($id,$TransactionResult, $SecurityToken, '');
					$retry = 0;
				} else {
					$TransErrorMsg = "";
				    foreach($xml->xpath('//soap:Fault') as $header) {
					  $TransErrorMsg = (string)$header->faultstring;
				    }
// <<soap:Server>>   <<Server was unable to process request. ---> The communication object, System.ServiceModel.Channels.ServiceChannel, 
//                      cannot be used for communication because it is in the Faulted state.>>
			        $failed += 1; 
$this->_logger->postIt('<<'.$TransErrorMsg.'>>',Zend_Log::ERR);
					if ( strlen($TransErrorMsg) > 100 ) $TransErrorMsg = substr($TransErrorMsg,0,100);
                    $this->updateTicketStatus($id,'9999',$SecurityToken, $TransErrorMsg);			
				}	
			} catch (Exception $e) {
				$failed += 1;
				if (isset($e->faultcode)) {
				   $msg = $e->faultstring;
				   if ( strlen($msg) > 100 ) $msg = substr($msg,0,100);
				} else {  
				   $msg = $e->getMessage();
				   if ( strlen($msg) > 36 ) $msg = substr($msg,0,100);
				}  
				$this->updateTicketStatus($id,$code,'',$msg);
				$this->_logger->postException($e);
			}	
// using this code to step through calls so if on is returned for expired auth token we can not increment, call (get_webSecurityToken()) then reprocess it		
//103	Authentication code is expired. $TransactionResult = "";   
			if ( strlen($TransactionResult) == 3 and $TransactionResult == "103" )  {
				if ($retry == 0) {
					$failed -= 1;
				    $retry += 1;
				} else {
					$this->updateTicketStatus($id,'103', "", "");
					break;
				}
			} else	{	
				$i+=1;
			} 
		} // end of while	  		
        $returnArray = array(
				'Selected'  => $numberSelected ,
				'Processed' => $processed,
				'Passed' 	=> $passed,
				'Failed'	=> $failed );
		return $returnArray;
	}
		
    function getAdminInformation()
	{
		try {
			$dbconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/sy_admin_properties.ini', APPLICATION_ENV);
			$dbAdmin= Zend_Db::factory( $dbconfig->db->admindb->adapter, $dbconfig->db->admindb->params->toArray() );			
			$dbAdmin->setFetchMode(Zend_Db::FETCH_OBJ);
    // Returns $result is a single object ... not an array of objects
			$sql = 'SELECT facilityRegNumber, webServiceID, webServicePW, state FROM sy_admin_clients WHERE applicationtag = ?';
			$result = $dbAdmin->fetchRow($sql,$this->_sess->applicationTag);	
			if (!$result){
				$logger->postIt('SY Administration Table did not return row for application tag<' . $this->_sess->applicationTag. '>', Zend_Log::INFO);
			} else {			
				$this->_sess->facilityRegNumber = $result->facilityRegNumber;
				$this->_sess->webServID = $result->webServiceID;
				$this->_sess->webServPW = $result->webServicePW;
				$this->_sess->webState = $result-> state;
//	$this->_logger->postIt(this->_sess->webServID.' '.$this->_sess->webServPW, Zend_Log::INFO); 
			}		
			
			$dbdbAdmin = null;
			
		} catch (Zend_Config_Exception $e) {
			$this->$logger->postException($e);
		}	
	}
	
	private function getTicketInfo( $ticket_id) {
		try {
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$options = $bootstrap->getOptions(APPLICATION_ENV);
			$dbconfig = $options['resources']['db'];
			$db = Zend_Db::factory($dbconfig['adapter'], $dbconfig['params']);
			$dateFormat = '%m/%d/%Y %r';
			$sql = 'select c.first_name,c.middle_name,c.last_name,c.address_1,c.address_2,c.city,' .
				"c.state,c.zip_code,c.picture,t.id,DATE_FORMAT(t.date_created,'".$dateFormat."') as fmtDate,t.tag,t.tag_state ".
				'from sy_tickets t, sy_customers c where t.customer_id = c.id and t.ticket_state = 1 '.
				'and t.id = ?';
//  $this->_logger->postIt('sql<' . $sql. '>', Zend_Log::INFO);
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			$result = $db->fetchRow($sql,$ticket_id);    
			if (!$result or is_null($result)) {
				$this->_logger->postIt('WebServiceClient.getTicketInfo Ticket and Customer info not found ticket<' . $ticket_id. '>', Zend_Log::INFO);
			}
			$db = null;
		} catch (Exception $e) {
			$this->_logger->postIt('Exception WebserviceClient.getTicketInfo '.$e->toString(), Zend_Log::INFO); 
		}	 
		return $result;
	}
	
	private function updateTicketStatus( $ticket_id,$CodeReturned,$AuthorizationToken,$SoapError) 
	{		
        $updateCode = $CodeReturned;
		$updateStatus = "";
		 If($CodeReturned == 1) { 
			$updateStatus = "Uploaded";
		} else {
			$updateStatus = "Error";		
		}
		try {
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$options = $bootstrap->getOptions(APPLICATION_ENV);
			$dbconfig = $options['resources']['db'];
			$db = Zend_Db::factory($dbconfig['adapter'], $dbconfig['params']);
            $table = 'sy_tickets';
			$data = array(
				'upload_status' =>  $updateStatus,
				'upload_error'  =>  $updateCode,
				'state_authorization_token' => $AuthorizationToken,
				'soap_error'    =>  $SoapError,
				'upload_date'   => new Zend_Db_Expr('NOW()') 
			);
            $row = $db->quoteInto('id =?', $ticket_id);
            $rowsAffected = $db->update($table, $data, $row);
			$this->_logger->postIt('Rows Updated:'.$rowsAffected, Zend_Log::INFO);
		
		
		} catch (Exception $e) {
			$this->_logger->postIt('Exception WebserviceClient.updateTicketStatus '.$e->toString(), Zend_Log::INFO); 
			$this->_logger->postException($e);
		}	 		
	}
		//$start_date = null, $end_date = null
	public function getTicketsToUpload($beginDate = null, $endDate = null, $scope = null) {
	    $result = null;
		$reportDate = $beginDate;

        if (is_null($beginDate)) { $beginDate = time(); }
        if (is_null($endDate)) { $endDate = $beginDate; }
        $datestr_start = $beginDate.' 00:00:00';
        $datestr_stop =  $endDate.' 23:59:59';
        if (is_null($scope)) { $scope = 'all'; }
//$this->_logger->postIt(' getTicketsToUpload('.$beginDate.', '.$endDate.', '.$scope.')', Zend_Log::INFO);
		try {

			$dateFormat = '%m/%d/%Y %r';
            $sql = "Select sy_tickets.customer_id,sy_tickets.corporate_ticket,DATE_FORMAT(sy_tickets.date_created,'%m/%d/%Y %r') as fmtDate, ".
                   "sy_tickets.id,sy_tickets.upload_status,sy_customers_corporate.name,sy_customers.first_name,sy_customers.last_name, ".
		           "sy_tickets.upload_error,sy_oh_web_errors.error_message, sy_tickets.soap_error ".
                   "from sy_tickets inner join sy_customers on sy_tickets.customer_id = sy_customers.id  ".
                   "left join sy_customers_corporate on sy_customers_corporate.id = sy_tickets.corporate_ticket ".
                   "left join sy_oh_web_errors on sy_oh_web_errors.error_code = sy_tickets.upload_error ".
                   "where  sy_tickets.ticket_state = 1  ". 
				   "and sy_tickets.date_created >= '".$datestr_start."'".  
				   "and sy_tickets.date_created <= '".$datestr_stop."'"	;			   
			$where = "and sy_tickets.upload_status != 'Uploaded' ";
			$orderBy = "order by sy_tickets.date_created";
	   
			if (strcasecmp($scope, 'all') == 0 ) {
				$statement = $sql.$orderBy; 
			} else {
				$statement = $sql.$where.$orderBy;
			}	
//$this->_logger->postIt('SQL '.$statement, Zend_Log::INFO);
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$options = $bootstrap->getOptions(APPLICATION_ENV);
			$dbconfig = $options['resources']['db'];
			$db = Zend_Db::factory($dbconfig['adapter'], $dbconfig['params']);
			$db->setFetchMode(Zend_Db::FETCH_NUM);
			$result = $db->fetchAll($statement);			
			if (!$result or is_null($result)) {
				$this->_logger->postIt('WebServiceClient.getTicketsToUpload No pending tickets found for '.$beginDate." to ".$endDate, Zend_Log::INFO);
			} else {	
			}		
		} catch (Exception $e) {
			$this->$logger->postException($e);
		}	
	    return $result;
	}
		
/* name        state_material_code    flg_spa  weight
Cast Aluminum	    11					0	    53
Irony Aluminum/SS	14					0		65
Sheet Iron	        19					0		251
ac compressor car	20					0		32
Hubs & Rotars	    20					0		284
*/
	private function getMaterialCodes( $ticket_id, $spa_Flag) {
		$materials = Array();	
		try {
			$sql = 'select m.name,m.state_material_code,m.flg_spa, p.gross from sy_purchases p, sy_metals m where p.metal_id = m.id ' .
                   'and p.ticket_id = '.$ticket_id.' order by m.flg_spa, m.state_material_code';
			$result = $this->get_Scrapyard_data ($sql, $ticket_id, 'array');
//$this->_logger->postIt('SQL '.$sql, Zend_Log::INFO);			
			if (!$result or is_null($result)) {
				$this->_logger->postIt('WebServiceClient.getMaterialCodes No Metals found<' . $ticket_id. '>', Zend_Log::INFO);
			} else {	
				foreach ($result as $row) {
					if ( $row[2] == $spa_Flag )	 {
						$materials[] = $row[1];
					}
				}  			
			}
		} catch (Exception $e) {
			$this->_logger->postException($e);
		}
        if (Count($materials) > 0 )	{	
			$returnAry = array_unique($materials);		
		} else {
		  return '';
		}
//$this->_logger->postIt('Returning:<' .implode(',', $returnAry).'>', Zend_Log::INFO);
		return implode(',', $returnAry);
	}

	private function calulateWeight($ticket) 
	{
		try {
			$sql = 'SELECT  ROUND( (SUM( p.gross ) - SUM( p.tare ) ) ) AS net_sum FROM sy_purchases p WHERE p.ticket_id = ?';
			$result = $this->get_Scrapyard_data ($sql, $ticket, 'row');
			if (!$result or is_null($result)) {
				$this->_logger->postIt('WebServiceClient.calulateWeight failed. SQL<'.$sql.'> Ticket: '.$ticket, Zend_Log::INFO);
				$returnVal = '';
			} else {	
//$this->_logger->postIt('weight from sql: '.$result->net_sum, Zend_Log::INFO);

// weight max five characters still waiting answer but it looks like no decimal place 999.99 (error 121) as of 2014_02_20 13:10 max five 99999
                $weight = (int) $result->net_sum;
				$returnVal = " ";
				if ( $weight > 99999 ) {
				  $returnVal = (string)'99999';
				} else {  
				   $returnVal = $result->net_sum;
				}	
				return $returnVal;
			}		
		} catch (Exception $e) {
			$this->_logger->postException($e);
		}	
	    return '';
	}
	// Exceptions to be handled by calling function
	private function get_Scrapyard_data ( $sql, $ticket, $ArrayOrRow) {
		$valueToReturn = null;
		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$options = $bootstrap->getOptions(APPLICATION_ENV);
		$dbconfig = $options['resources']['db'];
		$db = Zend_Db::factory($dbconfig['adapter'], $dbconfig['params']);
		if(strtolower(trim($ArrayOrRow)) == "row") {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$result = $db->fetchRow($sql,$ticket);			
		} else { 
			$db->setFetchMode(Zend_Db::FETCH_NUM);
			$result = $db->fetchAll($sql,$ticket);			
		}
		return $result;
	}
	
}
 ?>