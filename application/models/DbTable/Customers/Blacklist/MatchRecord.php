<?php

class SY_Model_DbTable_Customers_Blacklist_MatchRecord
{
	public $dl_state;
        public $dl_number;
        public $first_name;
        public $last_name;
        public $middle_name;
        public $date_of_birth;
        public $sex;
        private $conviction_case;
        private $conviction_text;
        private $conviction_date;
        
        /**
         * Looks up information about user, also loads info about conviction.
         * 
         * @return boolean true if on blacklist 
         */
        public function lookup() {
            
            $blacklist = new SY_Model_DbTable_Customers_Blacklist();
            // Check based on DL State/Number
            if( !empty($this->dl_number) || !empty($this->dl_number))
            {
                $select = $blacklist->select();
                if(!empty($this->dl_state))
                {
                    $select->where('dl_state = ?',$this->dl_state);
                }
                if(!empty($this->dl_number))
                {
                    $select->where('dl_number = ?',$this->dl_number);
                }
                $result = $select->query();
                if ($result->rowCount()) {
                    $this->setupResult($result->fetchObject());
                    return true;
                }
            }
            // Check based on First Name, Last Name and Middle Name, and DoB
            if( !empty($this->first_name) || 
                !empty($this->last_name) ||
                !empty($this->middle_name) ||
                !empty($this->date_of_birth))
            {
                $select = $blacklist->select();
                if(!empty($this->first_name))
                {
                    $select->where('first_name = ?',$this->first_name);
                }
                if(!empty($this->last_name))
                {
                    $select->where('last_name = ?',$this->last_name);
                }
                if(!empty($this->middle_name))
                {
                    $select->where('middle_name = ?',$this->middle_name);
                }
                if(!empty($this->date_of_birth))
                {
                    $select->where('date_of_birth = ?',$this->date_of_birth);
                }
                if ($this->sex) {
                    $select->where('sex = ?', $this->sex);
                    $select->orWhere('sex = ?', '');
                }
                $result = $select->query();
                if ($result->rowCount()) {
                    $this->setupResult($result->fetchObject());
                    return true;
                }
            }
            if( !empty($this->first_name) ||
                    !empty($this->last_name) ||
                    !empty($this->middle_name) ||
                    !empty($this->date_of_birth))
            {
                // Check based on First Name, Last Name, DoB and Middle Initial (in case db only has their middle initial)
                $select = $blacklist->select();
                if(!empty($this->first_name))
                {
                    $select->where('first_name = ?',$this->first_name);
                }
                if(!empty($this->last_name))
                {
                    $select->where('last_name = ?',$this->last_name);
                }
                if(!empty($this->middle_name))
                {
                    $select->where('middle_name = ?',substr($this->middle_name,0,1));
                }
                if(!empty($this->date_of_birth))
                {
                    $select->where('date_of_birth = ?',$this->date_of_birth);
                }
                if ($this->sex) {
                    $select->where('sex = ?', $this->sex);
                    $select->orWhere('sex = ?', '');
                }
                $result = $select->query();
                if ($result->rowCount()) {
                    $this->setupResult($result->fetchObject());
                    return true;
                }
            }
            return false;
        }
        
        private function setupResult($result) {
            $this->conviction_case = $result->conviction_case;
            $this->conviction_text = $result->conviction_text;
            $this->conviction_date = $result->conviction_date;
        }
        
        public function getCase() {
            return $this->conviction_case;
        }
        
        public function getCaseText() {
            return $this->conviction_text;
        }
        
        public function getCaseDate() {
            return $this->conviction_date;
        }
}
