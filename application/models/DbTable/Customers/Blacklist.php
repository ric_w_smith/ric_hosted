<?php

class SY_Model_DbTable_Customers_Blacklist extends Zend_Db_Table_Abstract
{
	protected $_name = 'sy_customers_blacklist';
        
        public function updateBLviaCSV($fpointer) {
            // now do the update
            rewind($fpointer);
            $header = fgetcsv($fpointer);
            foreach ($header as $k => $v) {
                $header[$k] = strtolower($v);
            }
            $header = array_flip($header);
            // Check for headers
            if (!isset($header['dl_state']))
                throw new Exception('No dl_state field in uploaded CSV');
            if (!isset($header['dl_number']))
                throw new Exception('No dl_number field in uploaded CSV');
            if (!isset($header['first_name']))
                throw new Exception('No first_name field in uploaded CSV');
            if (!isset($header['last_name']))
                throw new Exception('No last_name field in uploaded CSV');
            if (!isset($header['middle_initial']))
                throw new Exception('No middle_name field in uploaded CSV');
            if (!isset($header['last_name']))
                throw new Exception('No last_name field in uploaded CSV');
            if (!isset($header['dob']))
                throw new Exception('No dob field in uploaded CSV');
            //if (!isset($header['sex']))
                //throw new Exception('No sex field in uploaded CSV');
            if (!isset($header['conviction_case_number']))
                throw new Exception('No conviction_case field in uploaded CSV');
            if (!isset($header['conviction_text']))
                throw new Exception('No conviction_text field in uploaded CSV');
            if (!isset($header['conviction_date']))
                throw new Exception('No conviction_date field in uploaded CSV');
            // truncate the database table (after we have checked the headers)
            $sql = "TRUNCATE ".$this->getAdapter()->quoteTableAs($this->_name);
            $this->getAdapter()->getConnection()->exec($sql);
            $db = $this->getAdapter();
            $i = 0;
            $q = 0;
            $rows = array();
            while (($data = fgetcsv($fpointer)) !== FALSE) {
                try {
                ++$i;
                if ($i > 500) {
                    ++$q;
                    $sql = "INSERT INTO ".($db->quoteTableAs($this->_name))." (".
                            "dl_state, ".
                            "dl_number, ".
                            "first_name, ".
                            "last_name, ".
                            "middle_name, ".
                            "date_of_birth, ".
                            "conviction_case, ".
                            "conviction_text, ".
                            "conviction_date) ".
                            "VALUES ";
                     $sql .= implode(",\n ",$rows);
                     $rows = array();
                     $i = 0;
                     // die($sql);
                     // fputs($temp, $sql.";\n");
                     $db->getConnection()->exec($sql);
                     unset($sql);
                }
                } catch (Exception $e) {
                    die("Q: ".$q." SQL: ".$sql."\n");
                }
                // $this->insert($data);
                $row = array();
                $row[] = $db->quote($data[$header['dl_state']]);
                $row[] = $db->quote($data[$header['dl_number']]);
                $row[] = $db->quote($data[$header['first_name']]);
                $row[] = $db->quote($data[$header['last_name']]);
                $row[] = $db->quote($data[$header['middle_initial']]);
                $row[] = $db->quote($this->convertDate($data[$header['dob']], $q, $i));
                //$row[] = $db->quote($data[$header['sex']]);
                $row[] = $db->quote($data[$header['conviction_case_number']]);
                $row[] = $db->quote($data[$header['conviction_text']]);
                $row[] = $db->quote($this->convertDate($data[$header['conviction_date']], $q, $i));
                $row = "(".implode(',',$row).")";
                $rows[] = $row;
                unset($row);
            }
            
            $sql = "INSERT INTO ".($db->quoteTableAs($this->_name))." (".
                "dl_state, ".
                "dl_number, ".
                "first_name, ".
                "last_name, ".
                "middle_name, ".
                "date_of_birth, ".
                "conviction_case, ".
                "conviction_text, ".
                "conviction_date) ".
                "VALUES ";
            $sql .= implode(", ",$rows);
            $db->getConnection()->exec($sql);
        }
        
        private function convertDate($date, $q = 0, $i = 0) {
            if (!$date) return $date;
            $date = str_replace('//', '/', $date);
            $date_parts = explode('/',$date);
            if (count($date_parts == 2) && strlen($date_parts[0]) == 4) {
                $date_parts[2] = $date_parts[1];
                $date_parts[1] = substr($date_parts[0],2);
                $date_parts[0] = substr($date_parts[1],0,2);
            }
            if (count($date_parts) != 3) {
                print_r(array('date' => $date,'parts'=>$date_parts, 'q' => $q, 'i' => $i));
                die();
            }
            if (strlen($date_parts[2]) == 2) {
                if ($date_parts[2] > date('y')) {
                    $date_parts[2] = (substr(date('Y'),0,2)-1).$date_parts[2];
                } else {
                    $date_parts[2] = substr(date('Y'),0,2).$date_parts[2];
                }
            }
            $date = $date_parts[2].'-'.$date_parts[0].'-'.$date_parts[1];
            // echo $date."\n";
            return $date;
        }
}
