<?php
/**
 * Controls versioning for the most part, versions should always be 12 digits,
 * YYYYMMDDHHMM files should be stored in APPLICATION_PATH./sql-updates/YYYYMMDDHHSS.sql or .php
 */

class SY_Model_DbTable_Config extends Zend_Db_Table_Abstract
{

    protected $_name = 'sy_config';
    /**
     * Maximum seconds to wait on an update to finish before giving up and throwing an exception
     * @var type 
     */
    protected $_maxwait = 10;

    /**
     * @return  
     */
    protected function waitOnUpdate() {
        // Infinite till break.
        $query = $this->select(true)->where('name = ?','upgrading')->where('value = ?',1);
        $time = 0;
        $success = false;
        while ($time < $this->_maxwait) {
            $rows = $query->query();
            if ($rows->rowCount() == 0) {
                $success = true;
                break;
            } else {
                ++$time;
                sleep(1);
            }
        }
        if (!$success) {
            throw new Exception("Database update in progress. Please try again later.");
        }
    }
    
    public function doUpdate() {
        // Wait on update if someone else is doing it.
        $this->waitOnUpdate();
        $mtime = $this->select(true)->where('name = ?','update_mtime')->query()->fetch();
        $mtime = $mtime['value'];
        $updateStat = stat(APPLICATION_PATH.'/sql-updates');
        // die("last mtime: ".$mtime." dir mtime: ".$updateStat[9]); 1351911194
        if ($mtime < $updateStat[9]) {
            $version = $this->select(true)->where('name = ?','version')->query()->fetch();
            $version = $version['value'];
            // Set upgrade bit, but make sure we're the one doing it and someone else diddn't sneak in.
            // If they snuck in, it won't hurt to upgrade again AFTER they've completed.
            $n = 0;
            while ($n == 0) {
                $n = $this->update(array('value' => 1),'name = "upgrading"');
                if ($n == 0) {
                    $this->waitOnUpdate();
                }
            }
            $newversion = $this->findUpdates($version);
            $this->update(array('value' => $newversion),'name = "version"');
            $this->update(array('value' => $updateStat[9]),'name = "update_mtime"');
            $this->update(array('value' => 0),'name = "upgrading"');
        }
    }
    
    protected function findUpdates($oldversion) {
        $newversion = $oldversion;
        $handle = opendir(APPLICATION_PATH.'/sql-updates');
        if ($handle) {
            $updates = array();
            while (false !== ($file = readdir($handle))) {
                $filenum = substr($file,0,12);
                if (is_numeric($filenum)) {
                    $ext = substr($file,-4,4);
                    if ($ext == '.sql' || $ext == '.php') {
                        if ($filenum > $oldversion) {
                            $updates[] = $file;
                        }
                    }
                }
            }
            closedir($handle);
            asort($updates);
            foreach ($updates as $update) {
                $filenum = substr($update,0,12);
                if (is_numeric($filenum)) {
                    $newversion = $filenum;
                }
                $this->runner(APPLICATION_PATH.'/sql-updates/'.$update);
            }
        }
        return $newversion;
    }
    
    protected function runner($file) {
        $ext = substr($file,-4,4);
        if ($ext == '.sql') {
            $this->sqlRunner($file);
        } elseif ($ext == '.php') {
            $this->phpRunner($file);
        }
    }
    
    protected function sqlRunner($file) {
        $sql = file_get_contents($file);
        $db = $this->getAdapter();
        $db->getConnection()->exec($sql);
    }
    
    protected function phpRunner($file) {
        $db = $this->getAdapter();
        require $file;
    }
}
