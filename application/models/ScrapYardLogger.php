<?php

class ScrapYardLogger
{

	protected  $_logger;

   	public function __construct()  
	{  
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $options = $bootstrap->getOptions();
		$filename = $options['logpath'] . Date("Y") . "_" . Date("m") . "_" . Date("d") . "_Scrapyard.log";
		$writer = new Zend_Log_Writer_Stream($filename);
		$this -> _logger = new Zend_Log();
		$this -> _logger->addWriter($writer);
	}	
	
    public function postIt($message, $logLevel)
	{
		$this -> _logger->log($message, $logLevel);                                                                                                                       
	}
	// over-ride base class to ensure we all go thru here 
    public function log($message, $priority, $extras = null) {
		$this -> _logger->postIt($message, $logLevel);
	}
	
    public function postException($exception)
	{
		$this -> _logger->postIt('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',Zend_Log::ERR);                                                                                                                       
		if (isset($exception->faultcode)) {
			$this -> _logger->postIt('faultcode<'.$exception->faultcode.'>faultstring<'.$exception->faultstring.'>',Zend_Log::ERR); 
			$this -> _logger->postIt('faultcodes<'.$exception->faultcodens.'>',Zend_Log::ERR); 
		}
		$this -> _logger->postIt( $exception->getFile().' Line:'. $exception->getLine().' Error: '.$exception->getCode().' Msg: '.$exception->getMessage(),Zend_Log::ERR); 
		$this -> _logger->postIt('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',Zend_Log::ERR);                                                                                                                       
		$this -> _logger->postIt( ' Trace: '.$exception-> getTraceAsString(),Zend_Log::ERR); 
		$this -> _logger->postIt('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',Zend_Log::ERR);                                                                                                                       
	}	
}
 ?>