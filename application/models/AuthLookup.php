<?php

class SY_Model_AuthLookup
{
    private $_dbTable;
    
    function __construct() {
    	$this->_dbTable = new SY_Model_DbTable_Logins();
    }
    
    function lookupByUsername($username)
    {
    	// check for the existence of a user
    	$select = $this->_dbTable->select();
    	$select->where('username = ?', $username);
    	$userValid = $this->_dbTable->fetchRow($select);
    	if($userValid !== null)
    	{
    		return $userValid;
    	}
    	return null;
    }
    
    function lookupUser($username, $password)
    {
        // check for the existence of a user
        $select = $this->_dbTable->select();
        $select->where('username = ?', $username);
        $userValid = $this->_dbTable->fetchRow($select);
        if($userValid !== null)
        {
            $userData = $userValid->toArray();
            if($userData['username'] !== $username)
            {
                // username not valid, so return anyway
                return 'notFound';
            }
            // password has already been retrieved
            if(crypt($password, $userData['password']) == $userData['password'])
            {
                // the user returned is the same, so we're good to go
                return $userData;
            }
        }
        return false; // username and/or password not found
    }
}

