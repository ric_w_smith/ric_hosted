<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataTables
 *
 * @author wlightning
 */
class SY_Model_DataTables {
    /**
     * Does logic required for DataTables Server-Side Processing.
     * 
     * It is recommended to do in your view:
     * 	$this->_helper->layout()->disableLayout(); 
     *  $this->_helper->viewRenderer->setNoRender(true);
     * 
     * @param Zend_Db_Table $oTable The table being queried.
     * @param array $params Parameters (generally from GET)
     * @param array $columns columns you want returned.
     * @return string json ready for output 
     */
    function ajaxInteface(Zend_Db_Table_Abstract $oTable, $aParams, $aColumns) {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	//$aColumns = array( 'dl_state', 'dl_number', 'first_name', 'last_name', 'middle_name', 'date_of_birth', 'sex', 'conviction_case', 'conviction_date' );
	// $aColumns = $columns;
        
	/* Indexed column (used for fast and accurate table cardinality) */
	//$sIndexColumn = $dTable->info('primary');
        //print_r($sIndexColumn);
        //$sIndexColumn = $sIndexColumn[0];
	
	/* DB table to use */
        //$sTable = new SY_Model_DbTable_Customers_Blacklist();
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
        
        /*
         * Create select query...
         */
        $sQuery = $oTable->select(true);
	
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $aParams['iDisplayStart'] ) && $aParams['iDisplayLength'] != '-1' )
	{
            $sQuery->limit($aParams['iDisplayLength'], $aParams['iDisplayStart']);
		//$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
		//	mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	//$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) )
	{

		$sOrder = array();
		for ( $i=0 ; $i<intval( $aParams['iSortingCols'] ) ; $i++ )
		{
			if ( $aParams[ 'bSortable_'.intval($aParams['iSortCol_'.$i]) ] == "true" )
			{
                            $sOrder[] = $aColumns[ intval( $aParams['iSortCol_'.$i] ) ]." ".
				 	$aParams['sSortDir_'.$i];
				// $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
				// 	mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		if ($sOrder) {
                    $sQuery->order($sOrder);
                }
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	//$sWhere = "";
	if ( isset($aParams['sSearch']) && $aParams['sSearch'] != "" )
	{
		// $sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
                    $sQuery->orWhere($aColumns[$i].' LIKE ?','%'.$aParams['sSearch'].'%');
			// $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
		}
		// $sWhere = substr_replace( $sWhere, "", -3 );
		// $sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($aParams['bSearchable_'.$i]) && $aParams['bSearchable_'.$i] == "true" && $aParams['sSearch_'.$i] != '' )
		{
			//if ( $sWhere == "" )
			//{
			//	$sWhere = "WHERE ";
			//}
			//else
			//{
			//	$sWhere .= " AND ";
			//}
                        $sQuery->where($aColumns[$i].' LIKE ?','%'.$aParams['sSearch_'.$i].'%');
			// $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
        $sQuery->reset(Zend_Db_Select::COLUMNS);
        //$columns = $aColumns;
        //array_unshift($columns, new Zend_Db_Expr('SQL_CALC_FOUND_ROWS'));
        $sQuery->columns($aColumns);
        //  unset($columns);
	//$sQuery = "
	//	SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
	//	FROM   $sTable
	//	$sWhere
	//	$sOrder
	//	$sLimit
	//	";
	//$rResult = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
        // die($sQuery->assemble());
        $rResult = $sQuery->query(Zend_Db::FETCH_ASSOC);
	
	/* Data set length after filtering */
	//$sQuery = "
	//	SELECT FOUND_ROWS()
	//";
        $sQuery->reset(Zend_Db_Select::COLUMNS);
        $sQuery->reset(Zend_Db_Select::LIMIT_COUNT);
        $sQuery->reset(Zend_Db_Select::LIMIT_OFFSET);
        $sQuery->columns(new Zend_Db_Expr('COUNT(*)'));
        // $sQuery = $sTable->select()->reset(Zend_Db_Select::COLUMNS)->columns(new Zend_Db_Expr('FOUND_ROWS()'));
	$rResultFilterTotal = $sQuery->query(Zend_Db::FETCH_ASSOC);
	$aResultFilterTotal = $rResultFilterTotal->fetch(Zend_Db::FETCH_NUM); // mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	//$sQuery = "
	//	SELECT COUNT(`".$sIndexColumn."`)
	//	FROM   $sTable
	//";
        $sQuery = $oTable->select(true)->reset(Zend_Db_Select::COLUMNS)->columns(new Zend_Db_Expr('COUNT(*)'));
	$rResultTotal = $sQuery->query();
	$aResultTotal = $rResultTotal->fetch(Zend_Db::FETCH_NUM); // mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($aParams['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	//while ( $aRow = mysql_fetch_array( $rResult ) )
        foreach($rResult as $aRow)
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
		$output['aaData'][] = $row;
	}
	return json_encode( $output );
    }
}

?>
