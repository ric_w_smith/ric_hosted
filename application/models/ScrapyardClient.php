<?php

	class ScrapyardClient extends SoapClient {

		function __construct($wsdl, $options) {
			parent::__construct($wsdl, $options);
			$this->server = new SoapServer($wsdl, $options);
		}
		public function __doRequest($request, $location, $action, $version, $one_way = NULL) { 
			$result = parent::__doRequest($request, $location, $action, $version); 
			return $result; 
		} 
		function __anotherRequest($call, $params) {
			$location = 'https://ext.dps.state.oh.us/ScrapDealerTranService/ScrapDealerWebService.asmx';
			$action = 'http://tempuri.org/'.$call;
			$request = $params;
			$result =$this->__doRequest($request, $location, $action);
			return $result;
		} 
	}
	?>