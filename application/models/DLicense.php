<?php

/**
 * OH Driver license read/write model
 * Based on a passed-in string from a reader
 * Sample DL:
%OHDELAWARE^SMITH$BIB$D$^512 NORTH MAIN ST^?;6360231817535248=150219780228?#1043015 
 
Up to the first ^ is two letter state, then city.

Up to second ^ is last first middle, all separated by $

Next ^ is address in order, separated by spaces

Next is IIN, 10 characters (not used in testing)
Drivers License (6 characters) 525448 in mine (no letter prefixes)

Then = 1502 (SM, not used)
DOB = 19780228

after the #, last 5 is zip - 43015

Hope this helps!
 * @author ostin
 */
class SY_Model_DLicense {
    private $__origString;
    public $customer;

    public function __construct($string = null)
    {
        $this->customer = new SY_Model_Customer();
        if(!is_null($string))
        {
            $this->read($string);
        }
    }
    
    public function __get($name)
    {
        if(isset($this->customer->$name))
            return $this->customer->$name;
    }
    
    public function __set($name, $value)
    {
        $this->customer->$name = $value;
    }
    
    public function clear()
    {
        $this->__origString = '';
        $this->customer = new SY_Model_Customer();
    }
    //%OHGRANVILLE^KREBEHENNE$CANON$$^50 CHELSEA DR^?;6360231816243594=140719800705?#10430231096  D A             1509160BROHAZ                          ;;#/GV     ?
    public function read($string)
    {
        $this->__origString = $string;
        // do the parse
        try
        {
        // city/state first
        $data = explode('^', $string);
        if(!is_array($data) || count($data) < 4)
        {
            $this->clear();
            return;
        }
        $this->customer->state = substr($data[0], 1, 2);
        $this->customer->city = substr($data[0], 3);
        // name, split by $
        $namedata = explode('$', $data[1]);
        if(!is_array($namedata) || count($namedata) < 3)
        {
            throw new Exception();
        }
        $this->customer->last_name = $namedata[0];
        $this->customer->first_name = $namedata[1];
        $this->customer->middle_name = $namedata[2];
        // address
        $this->customer->address_1 = $data[2];
        // rest should be numbers
        $this->customer->issuerID = substr($data[3], 2, 10);
        $this->customer->drivers_license_number = substr($data[3], 12, 6);
        $setData = explode('=', $data[3]);
        if(!is_array($setData) || count($setData) < 2)
        {
            throw new Exception();
        }
        $this->customer->date_of_birth = substr($setData[1], 4, 4).'-'.substr($setData[1], 8, 2).'-'.substr($setData[1], 10, 2);
        $zipData = explode('?#', $setData[1]);
        if(!is_array($zipData) || count($zipData) < 2)
        {
            throw new Exception();
        }
        $this->customer->zip_code = trim(substr($zipData[1], 2, 10));
        $this->customer->scanned = true;
        }
        catch (Exception $e)
        {
            // probably don't have all the data
            // so do nothing
            $this->clear();
        }
    }
}
?>
