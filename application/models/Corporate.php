<?php

/**
 * Description of SY_Model_Customer
 *
 * @author ostin
 */
class SY_Model_Corporate extends SY_Model_Abstract {
    public $scanned = false;
    public $state, $date_created, $date_modified, $id;
    public $city, $name;
    public $address_1, $address_2, $zip_code;
    
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Corporate();
    }
    
    
    public function load($row)
    {
        
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && $varName !== 'scanned')
                $this->$varName = $row->$varName;
            }
    }

    public function validate(&$obj = null)
    {
        //if(is_object($obj) && get_class($obj) == 'SY_Model_DLicense')
        //{
        //    $obj = $obj->customer;
        //}
        return true;
    }
    
    public function getAllIds()
    {
        $select = $this->_db->select();
        $select->from($this->_db->getName(), 'id');
        $ids = $this->_db->fetchAll($select);
        $arr = array();
        foreach($ids as $aTick)
        {
            $arr[] = $aTick->id;
        }
        return $arr;
    }
    
    public function findAll()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== 'scanned' && $varName !== '_db' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $new = new SY_Model_Corporate();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }

   /* 
    public static function formatDOB($dob, $reverse = false)
    {
        if($reverse)
        {
            // going from db style to card style
            return substr($dob, 0, 4).substr($dob, 5, 2).substr($dob, 8);
        }
        if(is_numeric($dob) && strlen($dob) == 8)
        {
            return substr($dob, 0, 4).'-'.substr($dob, 4, 2).'-'.substr($dob, 6);
        }
        return '';
    }
    */

    public function beforeInsert(&$data)
    { }
    
    public function beforeUpdate(&$data)
    { }
    

    public function save()
    {
        $data = array();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                $data[$varName] = $this->$varName;
            }
            unset($data['scanned']);
            unset($data['_db']);
            $data['date_modified'] = new Zend_Db_Expr( 'NOW()' );
            if(isset($this->id) && is_numeric($this->id) && 0 != $this->id)
            {
                $this->_db->update($data, 'id = ' . $this->id);
            }
            else
            {
                $data['date_created'] = new Zend_Db_Expr('NOW()');
                $this->_db->insert($data);
                $this->id = $this->_db->getAdapter()->lastInsertId();
            }
    }

    public function delete()
    {
    }
    
    /**
     * Checks this customer against the blacklist table; if found, return true
     */
    /*
    public function isBlacklisted()
    {
        $bl = new SY_Model_DbTable_Customers_Blacklist_MatchRecord();
        $bl->dl_state = $this->state;
        $bl->date_of_birth = $this->date_of_birth;
        $bl->dl_number = $this->drivers_license_number;
        $bl->first_name = $this->first_name;
        $bl->last_name = $this->last_name;
        $bl->middle_name = $this->middle_name;
        return $bl->lookup();
    }
     */

    
    /**
     * Handles upload from passed-in upload info from form
     * @param array $upload
     */
    /*
    public function handlePictureUpload($upload)
    {
        // get the mimetype of the file
        $finfo = new finfo(FILEINFO_MIME);
        $type = $finfo->file($upload['tmp_name']);
        $mime = substr($type, 0, strpos($type, ';'));
        // set to scale the new image to width 150
        // get the image type first to figure out which PHP function to call
        $imageType = '';
        switch($mime)
        {
            case 'image/png':
                $imageType = 'png';
                break;
            case 'image/jpeg':
            case 'image/jpg':
                $imageType = 'jpg';
                break;
            case 'image/gif':
                $imageType = 'gif';
                break;
            default:
                $imageType = '';
                break;
        }
        // check the folder exist
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/customers/')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads/customers');
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/customers/' . $this->last_name . '.' . $this->first_name
                . '.' . $this->state . '.' . date('Ymd-His') . '.' . $upload['name'];
        move_uploaded_file( $upload['tmp_name'], $filename );
        // now do the scale
        switch($imageType)
        {
            case 'png':
                $newImage = imagecreatefrompng($filename);
                break;
            case 'jpg':
                $newImage = imagecreatefromjpeg($filename);
                break;
            case 'gif':
                $newImage = imagecreatefromgif($filename);
                break;
            default:
                $newImage = '';
                break;
        }
        if('' !== $newImage)
        {
            // do the work for good images
            $sourceX = imagesx($newImage);
            $sourceY = imagesy($newImage);
            $destX = 150;
            $destY = ($destX*$sourceY)/$sourceX; // did the math on this one :)
            $destImage = imagecreatetruecolor($destX, $destY);
            imagecopyresampled($destImage, $newImage, 0, 0, 0, 0, $destX, $destY, $sourceX, $sourceY);
            // make it a jpeg
            imagejpeg($destImage, $filename, 80);
            $this->picture_mimetype = 'image/jpeg';
            $this->picture = file_get_contents($filename);
        }
    }
     */
    
    public function __toString()
    {
        $output = new stdClass();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
        {
            if($varName !== '_db' && $varName !== 'picture')
            {
                $output->$varName = $this->$varName;
            }
        }
        return serialize($output);
    }
    
    public function toJSON()
    {
        $output = new stdClass();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
        {
            if($varName !== '_db' && $varName !== 'picture')
            {
                $output->$varName = $this->$varName;
            }
        }
        return json_encode($output);
    }

}

?>
