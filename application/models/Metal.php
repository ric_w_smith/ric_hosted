<?php

/**
 * Description of SY_Model_Metal
 *
 * @author ostin
 */
class SY_Model_Metal {
    protected $_db;
    public $id, $name, $price_per_unit, $date_modified, $active, $form_order;
    
    public function __construct($object = null)
    {
        if(!is_null($object) && $this->isArrayForm($object))
        {
            foreach(get_class_vars(get_class($this)) as $varName)
            {
                $this->$varName = $object[$varName];
            }
        }
        $this->_db = new SY_Model_DbTable_Metals();
    }
    
    private function isArrayForm($obj)
    {
        
        if(is_array($obj))
        {
            foreach($obj as $thisOne)
            {
                if(!in_array($thisOne, get_class_vars(get_class($this))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public function load($row)
    {
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db')
                $this->$varName = $row->$varName;
            }
    }
    
    public function findAll()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $new = new SY_Model_Metal();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }
    
    public function findOne()
    {
        $check = $this->findAll();
        if(isset($check[0])) return $check[0];
        return false;
    }
    
    public function save()
    {
        $data = array();
        foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                $data[$varName] = $this->$varName;
            }
            unset($data['_db']);
            if(isset($this->id) && is_numeric($this->id) && 0 != $this->id)
            {
                // copy the current data to the Metal_History table
                $history = new SY_Model_DbTable_Metals_History();
                $select = $this->_db->select();
                $select->where('id = ?', $this->id);
                $old = $this->_db->fetchRow($select);
                $olddata = array('metal_id' => $this->id, 'date' => $old->date_modified, 'price_per_unit' => $old->price_per_unit, 'active' => $old->active);
                $history->insert($olddata);
                $data['date_modified'] = new Zend_Db_Expr('NOW()');
                $this->_db->update($data, 'id = ' . $this->id);
            }
            else
            {
                $this->_db->insert($data);
                $this->id = $this->_db->getAdapter()->lastInsertId();
            }
    }
    
    public function getTicketList()
    {
        $select = $this->_db->select();
            foreach(get_class_vars(get_class($this)) as $varName => $varVal)
            {
                if($varName !== '_db' && !empty($this->$varName))
                {
                    $select->where("$varName = ?", $this->$varName);
                }
            }
        $select->where('active = ?','1');
        $select->order('form_order ASC');
        $found = $this->_db->fetchAll($select);
        $returns = array();
        foreach($found as $thisOne)
        {
            $new = new SY_Model_Metal();
            $new->load($thisOne);
            $returns[] = $new;
        }
        return $returns;
    }
    
}

?>
