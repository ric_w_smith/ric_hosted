<?php

class SY_Model_Client extends SY_Model_Abstract 
{
	public $id, $application_tag, $company_name, $status;    
    public $first_name, $middle_name, $last_name, $phone_number;
    public $address_1, $address_2, $city, $state, $zip_code;	
	public  $facilityRegNumber, $webServiceID, webServicePW;
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Clients();
    }
	    
	public function beforeInsert(&$data) {    }
    
    public function beforeUpdate(&$data) {    }
    
    public function validate(&$obj = null)
    {
        if(!is_null($obj) && is_array($obj))
        {
            foreach($obj as $key => $val)
            {
                if(!in_array($key, array_keys(get_class_vars(get_class($this)))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}

?>