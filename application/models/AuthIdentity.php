<?php

class SY_Model_AuthIdentity
{
    public $username;
    public $id;
    public $displayName;
    public $level;
    
    function __construct($id, $username, $displayName, $level) {
        $this->id = $id;
        $this->username = $username;
        $this->displayName = $displayName;
        $this->level = $level;
    }
    
    public function minLevel($level) {
        if ($level == 'office') {
            switch ($this->level) {
                case 'admin':
                case 'office':
                    return true;
                case 'yard':
                default:
                    return false;
            }
        } elseif ($level == 'yard') {
            switch ($this->level) {
                case 'admin':
                case 'office':
                    return true;
                case 'desk':
                default:
                    return false;
            }
        } elseif ($level == 'admin') {
            switch ($this->level) {
                case 'admin':
                    return true;
                case 'office':
                case 'yard':
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}

