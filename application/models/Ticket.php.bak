<?php

/**
 * Description of SY_Model_Ticket
 *
 * @author ostin
 */
class SY_Model_Ticket extends SY_Model_Abstract {
    public $id, $customer_id, $scale_id, $ticket_state, $epaform, $date_created, $sig_string, $tag_state,
	$tag, $login_id, $visittype, $TicketComments, $corporate_ticket;
    
    public function __construct($object = null)
    {
        parent::__construct($object);
        $this->_db = new SY_Model_DbTable_Tickets();
    }
    public function ticketNumber()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $options = $bootstrap->getOptions();
        return ((int) $this->id + $options['ticketOffset']);
    }
    
    /**
     * Returns all tickets with a non-existing client ID
     * @param array $clients Existing client IDs
     */
    public function getOrphaned(array $clients)
    {
        $select = $this->_db->select();
        if(count($clients) != 0)
        {
            $select->where('customer_id NOT IN (?)', $clients);
			$select->where('sy_tickets.ticket_state = ?', '0');
        }
        return $this->runSelect($select);
    }
    
    public function getAllOpen()
    {
        $select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_tickets');
        $select->join('sy_customers','sy_customers.id = sy_tickets.customer_id', array('last_name','first_name'));
        $select->where('sy_tickets.ticket_state = ?', '0');
        $select->order('sy_tickets.corporate_ticket', 'sy_customers.last_name', 'sy_customers.first_name');
        return $this->runSelect($select);
    }
    
    public function getAllClosed($startdate, $enddate, $scale = null, $login=null)
    {
		//format the time
		$datestr_start = date('Y-m-d', $startdate).' 00:00:00';
        $datestr_stop = date('Y-m-d', $enddate).' 23:59:59';
	
        $select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_tickets');
        $select->join('sy_customers','sy_customers.id = sy_tickets.customer_id', array('last_name','first_name'));
        $select->where('sy_tickets.ticket_state = ?', '1');
			$select->where('sy_tickets.date_created >= ?',$datestr_start);
	$select->where('sy_tickets.date_created <= ?',$datestr_stop);
	if ($scale)
	{
	    $select->where('sy_tickets.scale_id = ?', $scale);
	}
	if ($login)
	{ $select->where('sy_tickets.login_id = ?', $login);	}
	
        $select->order('sy_tickets.corporate_ticket', 'sy_customers.last_name','sy_customers.first_name');
		//echo $select;
        return $this->runSelect($select);
    }

    public function getCustomerClosed($customer, $corp = false)
    {
	$select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_tickets');
        $select->join('sy_customers','sy_customers.id = sy_tickets.customer_id', array('last_name','first_name'));
	$select->where('sy_tickets.ticket_state = ?', '1');
	if ($corp)
	{
	    $select->where('sy_tickets.corporate_ticket = ?',$customer);
	}
	else
	{
	    $select->where('sy_tickets.customer_id = ?',$customer);
	}
        $select->order('sy_tickets.date_created');
		//echo $select;
        return $this->runSelect($select);
    }
	
	public function getCustomerClosedTag($tag, $corp = false)
    {
	$select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_tickets');
        $select->join('sy_customers','sy_customers.id = sy_tickets.customer_id', array('last_name','first_name'));
	$select->where('sy_tickets.ticket_state = ?', '1');
	if ($corp)
	{
	    $select->where('sy_tickets.corporate_ticket = ?',$customer);
	}
	else
	{
	    $select->where('sy_tickets.tag = ?',$tag);
	}
        $select->order('sy_tickets.date_created');
		//echo $select;
        return $this->runSelect($select);
    }
    
    public function getAllDeleted()
    {
        $select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_tickets');
        $select->join('sy_customers','sy_customers.id = sy_tickets.customer_id', array('last_name','first_name'));
        $select->where('sy_tickets.ticket_state = ?', '3');
        $select->order('sy_customers.last_name','sy_customers.first_name');
        return $this->runSelect($select);
    }
    
    /**
     * $ total of all open tickets
     * 
     * @param int $date Date to query, unix timestamp format
     * @return float 
     */
    public function getTotalOpen($scale_id = null, $start_date = null, $end_date = null) {
        $select = $this->makeSelect($start_date, $end_date);
        $select->columns(array('total' => new Zend_Db_Expr('SUM((sy_purchases.gross - sy_purchases.tare)*sy_purchases.price_per_unit)')));
        $select->where('sy_tickets.ticket_state = 0');
        if(!is_null($scale_id))
        {
            $select->where('sy_tickets.scale_id = ?', $scale_id);
        }
        $result = $select->query();
        $row = $result->fetch();
        if (!$row)
            return '0.00';
        if (!$row['total'])
            return '0.00';
        return $row['total'];
    }
    
    private function makeSelect($start_date = null, $end_date = null)
    {
        if (is_null($start_date)) {
            $start_date = time();
        }
        if (is_null($end_date)) {
            $end_date = $start_date;
        }
        $datestr_start = date('Y-m-d', $start_date).' 00:00:00';
        $datestr_stop = date('Y-m-d', $end_date).' 23:59:59';

        $select = $this->_db->select();
        $select->setIntegrityCheck(false);
        $select->from('sy_purchases');
        $select->join('sy_tickets','sy_purchases.ticket_id = sy_tickets.id');
        $select->reset(Zend_Db_Select::COLUMNS); // I don't want any excess crap
        $select->where('sy_tickets.date_created >= ?',$datestr_start);
        $select->where('sy_tickets.date_created <= ?',$datestr_stop);
        $select->where('sy_tickets.ticket_state IN (?)', array('1','0'));
        return $select;
    }
    /**
     * $ total of all closed tickets
     * 
     * @param int $date Date to query, unix timestamp format
     * @return float
     */
    public function getTotalClosed($scale_id = null, $start_date = null, $end_date = null) {
        $select = $this->makeSelect($start_date, $end_date);
        $select->columns(array('total' => new Zend_Db_Expr('SUM((sy_purchases.gross - sy_purchases.tare)*sy_purchases.price_per_unit)')));
        $select->where('sy_tickets.ticket_state = 1');
        if(!is_null($scale_id))
        {
            $select->where('sy_tickets.scale_id = ?', $scale_id);
        }
        $result = $select->query();
        $row = $result->fetch();
        if (!$row)
            return '0.00';
        if (!$row['total'])
            return '0.00';
        return $row['total'];
    }
	
    
    /**
     * daily totals by metal
     * 
     * @param int $date Date to query, unix timestamp format
     * @return float
     */
    public function getMetalTotals($scale_id = null, $start_date = null, $end_date = null) {
        $select = $this->makeSelect($start_date, $end_date);
        $select->columns(
                array('sy_purchases.metal_id',
                    'sy_tickets.ticket_state',
                    'total_money' => new Zend_Db_Expr('SUM((sy_purchases.gross - sy_purchases.tare)*sy_purchases.price_per_unit)'),
                    'total_weight' => new Zend_Db_Expr('SUM(sy_purchases.gross - sy_purchases.tare)'),
                    ));
        // $select->where('sy_tickets.ticket_state = 1');
        $select->group(array('sy_tickets.ticket_state','sy_purchases.metal_id'));
        if(!is_null($scale_id))
        {
            $select->where('sy_tickets.scale_id = ?', $scale_id);
        }
        $result = $select->query();
        $metals = array();
        $metalmodel = new SY_Model_DbTable_Metals();
        foreach ($result as $row) {
            // Initialize metal
            if (!isset($metals[$row['metal_id']])) {
                $metals[$row['metal_id']] = array(
                    'open' => array(
                        'money' => '0.00',
                        'weight' => '0'
                        ),
                    'closed'=> array(
                        'money' => '0.00',
                        'weight' => '0'
                        ),
                    'detail' => $metalmodel->find($row['metal_id'])->current(),
                    );
            }
            $metals[$row['metal_id']][$row['ticket_state']?'closed':'open']['money'] = $row['total_money'];
            $metals[$row['metal_id']][$row['ticket_state']?'closed':'open']['weight'] = $row['total_weight'];
        }
        return $metals;
    }
    
    public function beforeInsert(&$data)
    {
        $data['date_created'] = new Zend_Db_Expr('NOW()');
    }
    
    public function findOpen()
    {
        if(!isset($this->customer_id) || $this->customer_id == 0) return false; // sanity check, yo
        // find any open ticket(s) for this user
        $select = $this->_db->select();
        $select->where('customer_id = ?', $this->customer_id);
        $select->where('ticket_state = ?', '0'); // 0 = open, 1 = closed ???
        return $this->runSelect($select);
    }
    
    public function beforeUpdate(&$data)
    {
        
    }
    
    private function runSelect($select)
    {
        $tickets = $this->_db->fetchAll($select);
        $ret = array();
        foreach($tickets as $thisTicket)
        {
            $c = get_class($this);
            $new = new $c();
            $new->load($thisTicket);
            $ret[] = $new;
        }
        if(isset($ret[0])) return $ret;
        return false;
    }

    public function validate(&$obj = null)
    {
        if(!is_null($obj) && is_array($obj))
        {
            foreach($obj as $key => $val)
            {
                if(!in_array($key, array_keys(get_class_vars(get_class($this)))))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
        /**
     * Handles upload from transaction
     * @param array $upload
     */
    public function uploadPicture($upload)
    {
        // get the mimetype of the file
        $finfo = new finfo(FILEINFO_MIME);
        $type = $finfo->file($upload['tmp_name']);
        $mime = substr($type, 0, strpos($type, ';'));
        // set to scale the new image to width 150
        // get the image type first to figure out which PHP function to call
        $imageType = '';
        switch($mime)
        {
            case 'image/png':
                $imageType = 'png';
                break;
            case 'image/jpeg':
            case 'image/jpg':
                $imageType = 'jpg';
                break;
            case 'image/gif':
                $imageType = 'gif';
                break;
            default:
                $imageType = '';
                break;
        }
        // check the folder exist
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/tickets/')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads/tickets');
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/tickets/' . $this->id . '.jpg';
        move_uploaded_file( $upload['tmp_name'], $filename );
        // now do the scale
        switch($imageType)
        {
            case 'png':
                $newImage = imagecreatefrompng($filename);
                break;
            case 'jpg':
                $newImage = imagecreatefromjpeg($filename);
                break;
            case 'gif':
                $newImage = imagecreatefromgif($filename);
                break;
            default:
                $newImage = '';
                break;
        }
        if('' !== $newImage)
        {
            // do the work for good images
            $sourceX = imagesx($newImage);
            $sourceY = imagesy($newImage);
            $destX = 150;
            $destY = ($destX*$sourceY)/$sourceX; // did the math on this one :)
            $destImage = imagecreatetruecolor($destX, $destY);
            imagecopyresampled($destImage, $newImage, 0, 0, 0, 0, $destX, $destY, $sourceX, $sourceY);
            // make it a jpeg
            imagejpeg($destImage, $filename, 80);
        }
    }

    public function getPicture()
    {
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/tickets/' . $this->id . '.jpg')) return file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/uploads/tickets/' . $this->id . '.jpg');
    }
    
    /**
     *  @return SY_Model_Customer
     */
    public function getCustomer() {
        $customer = new SY_Model_Customer();
        if ($this->customer_id) {
            $customer->id = $this->customer_id;
            $customer = $customer->findOne();
        }
        return $customer;
    }
}
