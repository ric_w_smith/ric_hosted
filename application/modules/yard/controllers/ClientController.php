<?php

class Yard_ClientController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function searchAction()
    {
        $params = $this->_request->getParams();
        unset($params['module']);
        unset($params['controller']);
        unset($params['action']);
        if(!empty($params))
        {
            $this->view->formvalues = $this->_request->getParams();
        }
    }
    
    private function loadCustomer($params)
    {
        $cu = new SY_Model_Customer();
        $cu->first_name = $params['firstName'];
        $cu->last_name = $params['lastName'];
        $cu->date_of_birth = SY_Model_Customer::formatDOB($params['dob']);
        $cu->drivers_license_number = $params['license'];
        $cust = $cu->findOne();
        return $cust;
    }
    
    public function resultsAction()
    {
        $params = $this->_request->getParams();
        // double check that there's the minimum requirements
        if(empty($params['scanlicense']) && (empty($params['firstName']) || empty($params['lastName']) || empty($params['dob'])))
        {
            unset($params['module']);
            unset($params['controller']);
            unset($params['action']);
            $outStr = '';
            foreach($params as $key => $val)
            {
                if('' != $val)
                $outStr .= "$key/$val/";
            }
            $this->_redirect('/yard/client/search/' . $outStr);
        }
        // check if it's already been verified
        if(isset($params['verified']) && 1 == $params['verified'])
        {
            // save the customer
            $sess = new Zend_Session_Namespace('scrapyard');
            $sess->customer = $this->loadCustomer($params);
            // open a ticket
            $this->_redirect('/yard/ticket/create');
        }
        // check if a valid license string
        if(isset($params['scanlicense']))
        {
            $licenseObj = new SY_Model_DLicense($params['scanlicense']);
            if($licenseObj->state == '')
            {
                $customer = new SY_Model_Customer();
                // invalid license, so we'll use the other data
                $customer->first_name = !empty($params['firstName']) ? $params['firstName'] : '';
                $customer->last_name = !empty($params['lastName']) ? $params['lastName'] : '';
                $customer->date_of_birth = !empty($params['dob']) ? substr($params['dob'], 0, 4).'-'.substr($params['dob'], 4, 2).'-'.substr($params['dob'], 6) : '';
                $customer->drivers_license_number = !empty($params['license']) ? $params['license'] : '';
            }
            else
            {
                $customer = new SY_Model_Customer($licenseObj);
            }
        }
        else
        {
            $customer = new SY_Model_Customer();
            // invalid license, so we'll use the other data
            $customer->first_name = !empty($params['firstName']) ? $params['firstName'] : '';
            $customer->last_name = !empty($params['lastName']) ? $params['lastName'] : '';
            $customer->date_of_birth = !empty($params['dob']) ? (substr($params['dob'], 0, 4).'-'.substr($params['dob'], 4, 2).'-'.substr($params['dob'], 6)) : '';
            $customer->drivers_license_number = !empty($params['license']) ? $params['license'] : '';
        }
        $this->view->customer = $customer->findOne();

        if($this->view->customer == false)
        {
            $params['yard'] = '1';
            $this->_redirector = $this->_helper->getHelper('Redirector');
            $this->_redirector->gotoSimple('results','client','office', $params);
        }
        
        if( false !== $this->view->customer && $this->view->customer->isBlacklisted() )
        {
            $this->view->blacklisted = true;
        }
        $this->view->formvalues = $params;
        
        
    }
    
    public function createAction()
    {
        
    }
}

