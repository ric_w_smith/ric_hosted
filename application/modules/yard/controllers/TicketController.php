<?php

class Yard_TicketController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {
	$type = $this->_request->getParam('corp', 0);
	$this->view->corpid = $type;

        // get the customer from the session
        $sess = new Zend_Session_Namespace('scrapyard');
        $cust = $sess->customer;
        // show the form by default
	$this->view->customer = $cust;
	$co = new SY_Model_Corporate();
	$co->id = $cust->corporate_affiliation?$cust->corporate_affiliation:-1;
	$this->view->corp = $co->findOne();
	
        // get the logged-in user
        $sess = new Zend_Session_Namespace('scrapyard_auth');
        $this->view->login_id = $sess->user->id;
        // load up the metals
        $metal = new SY_Model_Metal();
        $this->view->metals = $metal->getTicketList();
    }

}

