<?php

class Office_TransactionController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        // get the customer from the session
        $sess = new Zend_Session_Namespace('scrapyard');
        $cust = $sess->customer;
	// get any open tickets
	$this->view->tickets = null;
	if ($cust)
	{
            $tic = new SY_Model_Ticket();
            $tic->customer_id = $cust->id;
            $tics = $tic->findOpen();
	    $this->view->tickets = $tics;
	}
    }

    public function searchAction()
    {
        // get all orphaned transactions
        // start by getting a list of all client #s
        $cl = new SY_Model_Customer();
        $clientIds = $cl->getAllIds();
        $ti = new SY_Model_Ticket();
        $this->view->orphans = $ti->getOrphaned($clientIds);
        // default to only get open tickets
        $type = $this->_request->getParam('tickettype') ? $this->_request->getParam('tickettype') : 'open';
        switch($type)
        {
            case 'closed':
                $this->view->tickets = $ti->getAllClosed();
                break;
            case 'open':
            default:
                $this->view->tickets = $ti->getAllOpen();
                break;
        }
        $this->view->tickettype = $type;
    }
    

    public function viewAction()
    {
        // get the transaction ID from the parameters
        $params = $this->_request->getParams();
        if(!isset($params['id'])) $this->_redirect('/'); // redirect for bad data
        $tic = new SY_Model_Ticket();
        $tic->id = $params['id'];
        $tic = $tic->findOne();
        $pur = new SY_Model_Purchase();
        $pur->ticket_id = $tic->id;
        $purs = $pur->findAll();
        $this->view->ticket = $tic;
        // get the scale
        $scale = new SY_Model_Scale();
        $scale->id = $tic->scale_id;
        $scale = $scale->findOne();
        $this->view->scale = $scale->name;
        // get the logged-in user name if login_id !== 0
        if($tic->login_id != 0)
        {
            $login = new SY_Model_Login();
            $login->id = $tic->login_id;
            $login = $login->findOne();
            $this->view->login = $login->name;
        }
        // get any existing photo
        $this->view->picture = $tic->getPicture();
        // get all metals, too
        $metal = new SY_Model_Metal();
        $this->view->metals = $metal->findAll();
        $this->view->purchases = $purs;
	// $sess = new Zend_Session_Namespace('scrapyard');
        $this->view->customer = $tic->getCustomer(); // $sess->customer;
	$co = new SY_Model_Corporate();
	$co->id = $this->view->customer->corporate_affiliation?$this->view->customer->corporate_affiliation:-1;
	$this->view->corp = $co->findOne();

	$co2 = new SY_Model_Corporate();
	$this->view->allcorps = $co2->findAll();
    }

    public function convertAction()
    {
	$tickid = $this->_request->getParam('id', 0);
	$custid = $this->_request->getParam('customer_id', 0);
	$rep = $this->_request->getParam('representative', 0);
	if(!preg_match("/^[0-9]+$/i", $rep)) { $rep = 0; }

        $cl = new SY_Model_Customer();
	$cl->id = $custid;
	$cust = $cl->findOne();

	$ti = new SY_Model_Ticket();
	$ti->id = $tickid;
	$tick = $ti->findOne();
	if ($tick->ticket_state)
	{
	    $this->_helper->FlashMessenger->addMessage(array('message' => "Closed tickets cannot be converted.", 'status' => 'error'));
	    $this->_redirect('/office/transaction/view/id/' . $tickid);
	}

	if ($rep)
	{
	    $cust->corporate_affiliation = $rep;
	    $cust->save();
	}

	if ($tick->corporate_ticket)
	{
	    $tick->corporate_ticket = 0;
	}
	else
	{
	    $tick->corporate_ticket = $cust->corporate_affiliation;
	}
	$tick->save();
	$this->_redirect('/office/transaction/view/id/' . $tickid);
    }
    
    public function addpictoticketAction()
    {
        $params = $this->_request->getParams();
        if(isset($_FILES['picture']))
        {
            $tic = new SY_Model_Ticket();
            $tic->id = $params['id'];
            $tic = $tic->findOne();
            $tic->uploadPicture($_FILES['picture']);
            $tic->save();
        }
        $this->_redirect('/office/transaction/view/id/' . $params['id']);
    }
    
    public function addpictureAction()
    {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        
        /*
        	This receives the JPEG snapshot
            from webcam.swf as a POST request.
        */

        // We only need to handle POST requests:
        if( strtolower($_SERVER['REQUEST_METHOD']) != 'post' )
        {
        	exit;
        }
        $pathfrompublic = 'uploads/tickets';
        
        $folder = $_SERVER['DOCUMENT_ROOT'] . '/' . $pathfrompublic;
        
        $filename = $this->_request->getParam('id') . '.jpg';

        $original = $folder.'/'.$filename;

        // The JPEG snapshot is sent as raw input:
        $input = file_get_contents('php://input');

        if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be')
        {
            // Blank image. We don't need this one.
            exit;
        }

        $result = file_put_contents($original, $input);
        if (!$result)
        {
            // check the existence of folders & chmod status
            if(is_dir($folder))
            {
                chmod($folder, 0777);
                $result = file_put_contents($original, $input);
                if($result) break;
            }
            else
            {
                $folders = explode('/', $pathfrompublic);
                $curPath = '';
                foreach($folders as $thisFolder)
                {
                    if(!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $curPath . $thisFolder))
                    {
                        mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $curPath . $thisFolder);
                    }
                    chmod($_SERVER['DOCUMENT_ROOT'] . '/' . $curPath . $thisFolder, 0777);
                    $curPath .= $thisFolder . '/';
                }
                $result = file_put_contents($original, $input);
                if($result) break;
            	echo '{
            		"error"		: 1,
            		"message"	: "Failed save the image. Make sure you chmod the uploads folder and its subfolders to 777."
                }';
                exit;
            }
        }

        $info = getimagesize($original);
        if($info['mime'] != 'image/jpeg')
        {
        	unlink($original);
        	exit;
        }

        // Using the GD library to resize 
        // the image into a thumbnail:

        $origImage	= imagecreatefromjpeg($original);
        $newImage	= imagecreatetruecolor(400,250);
        imagecopyresampled($newImage,$origImage,0,0,0,0,400,250,520,370); 

        imagejpeg($newImage,$original);

        echo '{"status":1,"image":"' . base64_encode(file_get_contents($original)) . '","filename":"'.$filename.'"}';
    }

    
    public function editAction()
    {
        // get the transaction ID from the parameters
        $params = $this->_request->getParams();
        if(!isset($params['id'])) $this->_redirect('/'); // redirect for bad data
        $tic = new SY_Model_Ticket();
        $tic->id = $params['id'];
        $tic = $tic->findOne();
        $pur = new SY_Model_Purchase();
        $pur->ticket_id = $tic->id;
        $purs = $pur->findAll();
        $this->view->ticket = $tic;
        // get all metals, too
        $metal = new SY_Model_Metal();
        $this->view->metals = $metal->findAll();
        $this->view->purchases = $purs;
        // $sess = new Zend_Session_Namespace('scrapyard');
        $this->view->customer = $tic->getCustomer(); // $sess->customer;
        
        
    }
    
    public function updateAction()
    {
        // get the ticket ID
        $params = $this->_request->getParams();
        if(!isset($params['ticket_id'])) $this->_redirect('/'); // redirect for bad data
        $tic = new SY_Model_Ticket();
        $tic->id = $params['ticket_id'];
	$tic = $tic->findOne();
	$tic->date_created = date('Y-m-d H:i:s', strtotime($params['date']));
	$tic->tag_state = $this->_request->getParam('tag_state', '');
	$tic->tag = $this->_request->getParam('tag', 0);
	$tic->save();
        $metal = new SY_Model_Metal();
        $allMetals = $metal->findAll();
        for($j = 1; $j <= $allMetals[count($allMetals)-1]->id; $j++)
        {
            if (!isset($params['gross'.$j])) {
                $params['gross'.$j] = 0;
            }
            if (!isset($params['tare'.$j])) {
                $params['tare'.$j] = 0;
            }
            if($params['gross'.$j] > 0 && $params['gross'.$j] > $params['tare'.$j])
            {
                $data['ticket_id'] = $tic->id;
                $data['metal_id'] = $j;
                $purchase = new SY_Model_Purchase($data);
                $purchase = $purchase->findOne();
                if($purchase == false)
                {
                    $purchase = new SY_Model_Purchase($data);
                }
                $purchase->gross = $params['gross'.$j];
                $purchase->tare = $params['tare'.$j];
		//$purchase->price_per_unit = $params['ppu'.$j];
		$purchase->price_per_unit = isset($params['ppu'.$j]) ? $params['ppu'.$j] : $params['ppuorig'.$j];
                $purchase->save();
                unset($purchase);
            } else {
                $data['ticket_id'] = $tic->id;
                $data['metal_id'] = $j;
                $purchase = new SY_Model_Purchase($data);
                $purchase = $purchase->findOne();
                if($purchase != false)
                {
                    $purchase->delete();
                }
            }
        }
        $this->_redirect('/office/transaction/view/id/' . $tic->id);
    }
    
    public function closeAction()
    {
        $params = $this->_request->getParams();
        if(!isset($params['id']))
        {
            SY_Plugin_Log::log('id param not set for office/transaction/close');
            $this->_redirect('/'); // redirect for bad data
        }
        $tick = new SY_Model_Ticket();
        $tick->id = $params['id'];
        $tick = $tick->findOne();
        if (isset($params['sig_string']) && $params['sig_string']) {
            SY_Plugin_Log::log('sig string: ' . $params['sig_string']);
            $tick->sig_string = $params['sig_string'];
        }
        $tick->ticket_state = 1;
        $tick->save();
        $this->_redirect('/office/transaction');
    }
    
    public function deleteAction()
    {
        $params = $this->_request->getParams();
        if(!isset($params['id']))
        {
            SY_Plugin_Log::log('id param not set for office/transaction/delete');
            $this->_redirect('/'); // redirect for bad data
        }
        $tick = new SY_Model_Ticket();
        $tick->id = $params['id'];
        $tick = $tick->findOne();
        $tick->ticket_state = 3;
        $tick->save();
        $this->_redirect('/office/transaction');
    }
}

