<?php

class Office_ClientController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function searchAction()
    {
        $paras = $this->_request->getParams();
        if(isset($paras['created']) && $paras['created'] == 1)
        {
            $this->view->created = 1;
        }
    }
    
    public function webcamAction()
    {
        //disable view & controller
        $this->_helper->layout()->disableLayout(); 
        //$this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function sendAction()
    {
        
    }
    
    public function resultsAction()
    {
        $params = $this->_request->getParams();
        // check if a valid license string
        if(isset($params['scanlicense']))
        {
            $licenseObj = new SY_Model_DLicense($params['scanlicense']);
            if($licenseObj->state == '')
            {
                $customer = new SY_Model_Customer();
                // invalid license, so we'll use the other data
                $customer->first_name = !empty($params['firstName']) ? $params['firstName'] : '';
                $customer->last_name = !empty($params['lastName']) ? $params['lastName'] : '';
                $customer->date_of_birth = !empty($params['dob']) ? $customer->formatDOB($params['dob']) : '';
                $customer->drivers_license_number = !empty($params['license']) ? $params['license'] : '';
            }
            else
            {
                $customer = new SY_Model_Customer($licenseObj);
                // set the params vars as needed
                foreach(get_class_vars('SY_Model_Customer') as $key => $val)
                {
                    $params[$key] = $customer->$key;
                }
            }
        }
        else
        {
            $customer = new SY_Model_Customer();
            // invalid license, so we'll use the other data
            $customer->first_name = !empty($params['firstName']) ? $params['firstName'] : '';
            $customer->last_name = !empty($params['lastName']) ? $params['lastName'] : '';
            $customer->date_of_birth = !empty($params['dob']) ? $customer->formatDOB($params['dob']) : '';
            $customer->drivers_license_number = !empty($params['license']) ? $params['license'] : '';
        }
        $this->view->customer = $customer->findOne();
        if( $customer->isBlacklisted() )
        {
            $this->view->blacklisted = true;
        }
        else
        {
            // was this client accepted?
            if(!isset($params['scanlicense']) && $params['verified'] == 1)
            {
                // is there an open ticket?
                $ticket = new SY_Model_Ticket();
                $ticket->customer_id = $this->view->customer->id;
                $tickets = $ticket->findOne();
                if(false !== $tickets)
                {
                    // open ticket
                    
                }
                else
                {
                    // no open ticket, send them to yard
                    $this->_redirect('/yard/client/');
                }
            }
            elseif(!isset($params['scanlicense']))
            {
                // were changes made?
                if(isset($params['makechanges']) && $params['makechanges'] == 1)
                {
                    // review new data
                }
                elseif(isset($params['makechanges']))
                {
                    // no changes made, so rejected
                    $this->_redirect('/office/client/search');
                }
            }
        }
        if(isset($this->view->customer->id) && is_numeric($this->view->customer->id))
        {
            $ticket = new SY_Model_Ticket();
            $ticket->customer_id = $this->view->customer->id;
            $this->view->opentickets = $ticket->findOpen();
        }
        $sess = new Zend_Session_Namespace('scrapyard');
        $sess->customer = $this->view->customer;
        $this->view->formvalues = $params;
    }
    
    public function createAction()
    {
        $params = $this->_request->getParams();
        $cust = new SY_Model_Customer();
        $cust->first_name = $params['firstName'];
        $cust->last_name = $params['lastName'];
        $cust->middle_name = $params['middleName'];
        $cust->address_1 = $params['address_1'];
        $cust->address_2 = $params['address_2'];
        $cust->date_of_birth = $params['dob'];
        $cust->drivers_license_number = $params['license'];
        $cust->city = $params['city'];
        $cust->state = $params['state'];
        $cust->zip_code = $params['zip'];
        if(isset($_SESSION['picture'])) {
        	$cust->picture_mimetype = 'image/jpeg';
        	$cust->picture = $_SESSION['picture'];
        }
        $cust->save();
        if(isset($params['yard']))
        {
            // redirect to the yard to start the ticket
            $sess = new Zend_Session_Namespace('scrapyard');
            $sess->customer = $cust;
            // open a ticket
            $this->_redirector = $this->_helper->getHelper('Redirector');
            $this->_redirector->gotoSimple('create','ticket','yard');
        }
        else
        {
            // send them down the line
            $this->_redirector = $this->_helper->getHelper('Redirector');
            $this->_redirector->gotoSimple('search','client','office',array('created' => 1));
        }
    }
    
    public function updateAction()
    {
        $params = $this->_request->getParams();
        $oldCust = new SY_Model_Customer();
        if(isset($params['oldvalues']) && null !== $params['oldvalues'])
        {
            $oldCustObj = unserialize($params['oldvalues']);
            // load the old customer
            $oldCust->load($oldCustObj);
            $cust = $oldCust->findOne(); // get the old customer from the values
        }
        else
        {
            $cust = new SY_Model_Customer();
        }
        $cust->first_name = $params['firstName'];
        $cust->last_name = $params['lastName'];
        $cust->middle_name = $params['middleName'];
        $cust->address_1 = $params['address_1'];
        $cust->address_2 = $params['address_2'];
        $cust->date_of_birth = $params['dob'];
        $cust->drivers_license_number = $params['license'];
        $cust->city = $params['city'];
        $cust->state = $params['state'];
        $cust->zip_code = $params['zip'];
        
        
        session_start();
        
        if(isset($_SESSION['picture'])) {
        	$cust->picture_mimetype = 'image/jpeg';
        	$cust->picture = $_SESSION['picture'];
        }
        
        $cust->save();
        $sess = new Zend_Session_Namespace('scrapyard');
        $sess->customer = $cust;
        if(isset($params['yard']))
        {
            // redirect to the yard to start the ticket
            // open a ticket
            $this->_redirector = $this->_helper->getHelper('Redirector');
            $this->_redirector->gotoSimple('create','ticket','yard');
        }
        else
        {
            // send them down the line
            $this->_redirector = $this->_helper->getHelper('Redirector');
            $this->_redirector->gotoSimple('index','transaction','office');
        }
    }
    
    public function uploadAction() {		
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $_FILES['file']['tmp_name']);
		finfo_close($finfo);
		// set to scale the new image to width 150
		// get the image type first to figure out which PHP function to call
		$imageType = '';
		switch($mime)
		{
		    case 'image/png':
		        $imageType = 'png';
		        break;
		    case 'image/jpeg':
		    case 'image/jpg':
		        $imageType = 'jpg';
		        break;
		    case 'image/gif':
		        $imageType = 'gif';
		        break;
		    default:
		        $imageType = '';
		        break;
		}
		// check the folder exist
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/customers/')) mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads/customers');
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/customers/' . date('Ymd-His') . '.' . $_FILES['file']['name'];
		move_uploaded_file( $_FILES['file']['tmp_name'], $filename );
		// now do the scale
		switch($imageType)
		{
		    case 'png':
		        $newImage = imagecreatefrompng($filename);
		        break;
		    case 'jpg':
		        $newImage = imagecreatefromjpeg($filename);
		        break;
		    case 'gif':
		        $newImage = imagecreatefromgif($filename);
		        break;
		    default:
		        $newImage = '';
		        break;
		}
		if('' !== $newImage)
		{
		    // do the work for good images
		    $sourceX = imagesx($newImage);
		    $sourceY = imagesy($newImage);
		    $destX = 400;
		    $destY = ($destX*$sourceY)/$sourceX; // did the math on this one :)
		    $destImage = imagecreatetruecolor($destX, $destY);
		    imagecopyresampled($destImage, $newImage, 0, 0, 0, 0, $destX, $destY, $sourceX, $sourceY);
		    // make it a jpeg
		    imagejpeg($destImage, $filename, 80);
		    session_start();
		    
		    $_SESSION['picture'] = file_get_contents($filename);
		}
		
		
//    	$params = $this->_request->getParams();
//    	$oldCust = new SY_Model_Customer();
//    	if(isset($params['oldvalues']) && null !== $params['oldvalues'])
//    	{
//    	    $oldCustObj = unserialize($params['oldvalues']);
    	    // load the old customer
//    	    $oldCust->load($oldCustObj);
//    	    $cust = $oldCust->findOne(); // get the old customer from the values
//    	}
//    	else
//    	{
//    	    $cust = new SY_Model_Customer();
//    	}
//    
//    	$cust->handlePictureUpload($_FILES['file']);
    	
    }
}

