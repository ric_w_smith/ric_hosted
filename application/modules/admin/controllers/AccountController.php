<?php

class Admin_AccountController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function dayAction()
    {
        // let's see if we're a post
        $params = $this->_request->getParams();
        if(isset($params['balance']) && is_numeric($params['balance']) && isset($params['txtDate']))
        {
        
        	//check date format
        	$d = DateTime::createFromFormat('Y-m-d', $params['txtDate']);
            
            if($d && $d->format('Y-m-d') == $params['txtDate']) {
	            $ac = new SY_Model_Account();
				//check to see if record exists for given date
	
		        $ac->date = $params['txtDate'];
		        $acs = $ac->findOne();
	        	
	        	if($acs !== false)
		        {
					$ac->id = $acs->id;
		        }
	        	
	        	
	        	unset($acs);
	        	
	            $ac->balance = $params['balance'];			
				
	            $ac->save();
	            unset($ac);
	            
	            $this->view->balanceMessage = "A balance of <strong>$" . number_format($params['balance'], 2, '.', ',') . "</strong> was added for <strong>" . $params['txtDate'] . "</strong>";        
	            
	            $this->view->date = $params['txtDate'];
            }
            else {
            	$this->view->balanceMessage = "<strong>Invalid input. Please try again.";        
            }
            
        }
        else {
        	$this->view->date = date('Y-m-d');
        }        
    }
        
}

