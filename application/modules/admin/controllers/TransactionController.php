<?php

class Admin_TransactionController extends App_Controller_Action
{
	protected $logger;

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
		Zend_Loader::loadClass('WebserviceClient', APPLICATION_PATH."/models");
		$this->_logger = new ScrapYardLogger();
	}

    public function indexAction()
    {
        // action body
    }

    public function searchAction()
    {
        // get all orphaned transactions
        // start by getting a list of all client #s
        $cl = new SY_Model_Customer();
        $clientIds = $cl->getAllIds();
        $ti = new SY_Model_Ticket();
        $this->view->orphans = $ti->getOrphaned($clientIds);
        // default to only get open tickets
        $type = $this->_request->getParam('tickettype') ? $this->_request->getParam('tickettype') : 'open';
        switch($type)
        {
            case 'closed':
				//check to see if we have values, otherwise set start and end dates to today
					 if ($this->getRequest()->getParam('datestart',null)) {
						$beginDate = strtotime($this->getRequest()->getParam('datestart',null));
					} else {
						$beginDate = strtotime(date('Y-m-d'));
					}
					$this->view->start_date = $beginDate;
					
					if ($this->getRequest()->getParam('dateend',null)) {
						$endDate = strtotime($this->getRequest()->getParam('dateend',null));
					} else {
						$endDate = $beginDate;
					}
					$this->view->end_date = $endDate;
					
					$this->view->tickets = $ti->getAllClosed($beginDate,$endDate);
                break;
            case 'open':
            default:
                $this->view->tickets = $ti->getAllOpen();
                break;
        }
        $this->view->tickettype = $type;
    }
    
    public function viewAction()
    {
        
    }

    public function findticketAction()
    {
	$number = $this->_request->getParam('number', null);

	if(!preg_match("/^[0-9]+$/i", $number))
	{
        $this->_helper->FlashMessenger->addMessage(array('message' => "Invalid ticket number.", 'status' => 'error'));	
	    $this->_logger->postIt('search for invalid ticket number');
	    $this->_redirect('/admin/transaction/search'); // redirect for bad data
	}

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $options = $bootstrap->getOptions();
	$id = ((int) $number - $options['ticketOffset']);

        $ti = new SY_Model_Ticket();
        $ti->id = $id;
	if($ti = $ti->findOne())
	{
	    $this->_redirect('/office/transaction/view/id/' . $ti->id);
	}

        $this->_helper->FlashMessenger->addMessage(array('message' => "No ticket found.", 'status' => 'error'));	
	$this->_redirect('/admin/transaction/search');
    }

	
	    public function findticketbylicenseAction()
    {
	$number = $this->_request->getParam('plate', null);

        $ti = new SY_Model_Ticket();
        $ti->tag = $number;
			if($ti = $ti->findOne())
			{
				$this->_redirect('/admin/client/tickets/tag/' . $ti->tag);
			}

        $this->_helper->FlashMessenger->addMessage(array('message' => "No ticket found for this license plate.", 'status' => 'error'));	
		$this->_redirect('/admin/transaction/search');
    }
	
	
        public function editAction()
    {
        // get the transaction ID from the parameters
        $params = $this->_request->getParams();
        if(!isset($params['id'])) $this->_redirect('/'); // redirect for bad data
        $tic = new SY_Model_Ticket();
        $tic->id = $params['id'];
        $tic = $tic->findOne();
        $pur = new SY_Model_Purchase();
        $pur->ticket_id = $tic->id;
        $purs = $pur->findAll();
        $this->view->ticket = $tic;
        // get all metals, too
        $metal = new SY_Model_Metal();
        $this->view->metals = $metal->findAll();
        $this->view->purchases = $purs;
        // $sess = new Zend_Session_Namespace('scrapyard');
        $this->view->customer = $tic->getCustomer(); // $sess->customer;
    }
    
    public function updateAction()
    {
        // get the ticket ID
        $params = $this->_request->getParams();
        if(!isset($params['ticket_id'])) $this->_redirect('/'); // redirect for bad data
        $tic = new SY_Model_Ticket();
        $tic->id = $params['ticket_id'];
        $tic = $tic->findOne();
        $metal = new SY_Model_Metal();
        $allMetals = $metal->findAll();
        for($j = 1; $j <= $allMetals[count($allMetals)-1]->id; $j++)
        {
            if (!isset($params['gross'.$j])) {
                $params['gross'.$j] = 0;
            }
            if (!isset($params['tare'.$j])) {
                $params['tare'.$j] = 0;
            }
            if($params['gross'.$j] > 0 && $params['gross'.$j] > $params['tare'.$j])
            {
                $data['ticket_id'] = $tic->id;
                $data['metal_id'] = $j;
                $purchase = new SY_Model_Purchase($data);
                $purchase = $purchase->findOne();
                if($purchase == false)
                {
                    $purchase = new SY_Model_Purchase($data);
                }
                $purchase->gross = $params['gross'.$j];
                $purchase->tare = $params['tare'.$j];
                $purchase->price_per_unit = $params['ppu'.$j];
                $purchase->save();
                unset($purchase);
            }
        }
        $this->_redirect('/office/transaction/view/id/' . $tic->id);
    }
    

    public function viewallAction()
    {
        // get the customer id from the parameters
        $params = $this->_request->getParams();
        if(!isset($params['customer_id'])) $this->_redirect('/admin/transaction/search');
        // get all transactions with that customer id
        $ti = new SY_Model_Ticket();
        $ti->customer_id = $params['customer_id'];
        $tickets = $ti->findAll();
        $this->view->tickets = $tickets;
        $cu = new SY_Model_Customer();
        $cu->id = $params['customer_id'];
        $this->view->customer = $cu->findOne();
    }

    public function deleteOrphanAction()
    {
	$id = $this->_request->getParam('id', null);
	echo $id;
	if(!preg_match("/^[0-9]+$/i", $id))
        {
            $this->_helper->FlashMessenger->addMessage(array('message' => "Invalid ticket number.", 'status' => 'error'));	
            //$this->_logger->postIt('id param not set for orphan delete');
            $this->_redirect('/admin/transaction/search'); // redirect for bad data
	}

        $cl = new SY_Model_Customer();
	$clientIds = $cl->getAllIds();

        $tick = new SY_Model_Ticket();
        $tick->id = $id;
	$tick = $tick->findOne();
	//if(in_array($tick->customer_id, $clientIds))
	//{
            //$this->_logger->postIt('attempted deletion of non-orphan by orphan delete');
            //$this->_redirect('/admin/transaction/search'); // redirect for bad data
	//}
        $tick->ticket_state = 3;
        $tick->save();
		 $this->_helper->FlashMessenger->addMessage(array('message' => $id, 'status' => 'error'));	
        $this->_redirect('/admin/transaction/search');
    }

	public function uploadAction() {
		
		$params = $this->_request->getParams();
        if(isset($params['nextAction']))
			$nextAction = $params['nextAction'];
		else    
		   $nextAction = '';
		
		$webService = new WebserviceClient();		
		$beginDate = null;		
		$endDate = null;	
		if ( $this->getRequest()->isPost() and $nextAction == 'upload' ) {	
			if( !isset( $_POST['selectedTickets'] ) or empty( $_POST['selectedTickets'] )) {
				$this->view->message =	"Please select ticket(s)";
				$this->view->ticketInfo = $webService->getTicketsToUpload($params['begin_Date'],$params['end_Date'],"Pending");													
			} else {
				$ticketIDs = $_POST['selectedTickets'];
				$Results = $webService->uploadTickets($ticketIDs);
				$this->view->message = " Selected: ".$Results['Selected']." Processed: ".$Results['Processed']." Passed: ".$Results['Passed']." Failed: ".$Results['Failed'];
				$this->view->nextAction = 'query';
				$this->view->ticketInfo = $webService->getTicketsToUpload($params['begin_Date'],$params['end_Date'],"Pending");									
			}			
		} else {
			if ( $nextAction == 'query' ) {
				$beginDate = $params['begin_Date'];
				$endDate = $params['end_Date'];

			} else {
				//First time open url ... if no startthrough no begin date set ..... default		
				$beginDate = date('Y-m-d');
				$endDate = date('Y-m-d');
			}
			$this->view->ticketInfo = $webService->getTicketsToUpload($beginDate,$endDate,"Pending");	
			$this->view->message = "";    
		}	
	}
}

 ?>