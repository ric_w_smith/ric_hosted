<?php

class Admin_UsersController extends App_Controller_Action
{

	private $_logger, $_sess;
    public function init()
    {
        /* Initialize action controller here */
        parent::init();
		Zend_Loader::loadClass('WebserviceClient', APPLICATION_PATH."/models");
		$this->_sess = new Zend_Session_Namespace('scrapyard');
		if ( !isset( $this->_sess->scrapLog) ) 	{	
			$this->_sess->scrapLog = new ScrapYardLogger();
		}	
		$this->_logger = $this->_sess->scrapLog;
	}
    public function indexAction()
    {
        // make sure we're an admin
        $auth = Zend_Auth::getInstance()->getIdentity();
        if(!$auth->minLevel('admin'))
            $this->_redirect('/');
        // show all the users
        $logs = new SY_Model_Login();
        $this->view->logins = $logs->findAll();
    }
    
    public function deleteAction() {
        $user = new SY_Model_Login();
        $user->id = $this->getRequest()->getParam('user_id');
        $user = $user->findOne();
        $user->delete();
        $this->_redirect($this->view->url(array('action' => 'index', 'user_id' => null)));
    }
    
    public function setpasswordAction() {
        $user = new SY_Model_Login();
        $user->id = $this->getRequest()->getParam('user_id');
        $user = $user->findOne();
        if ($this->getRequest()->isPost()) {
            $user->password = crypt($this->getRequest()->getParam('password'));
            $user->save();
            $this->_redirect($this->view->url(array('action' => 'index', 'user_id' => null)));
        } else {
            $this->view->user = $user;
        }
    }
    
    public function addAction() {
        $user = new SY_Model_Login();
        if ($this->getRequest()->isPost()) {
            $user->username = $this->getRequest()->getParam('username');
            $user->name = $this->getRequest()->getParam('name');
            $user->suspended = $this->getRequest()->getParam('suspended')?1:0;
            $user->level = $this->getRequest()->getParam('level');
            $user->resetpass = 0;
            $pass = $this->getRequest()->getParam('password');
            if (!$pass) {
                $user->password = null;
            } else {
                $user->password = crypt($pass);
            }
            $user->save();
            $this->_redirect($this->view->url(array('action' => 'index', 'user_id' => null)));
        } else {
            $this->view->user = $user;
        }
    }
    
    public function editAction() {
        $user = new SY_Model_Login();
        $user->id = $this->getRequest()->getParam('user_id');
        $user = $user->findOne();
        if ($this->getRequest()->isPost()) {
            // Cannot update username
            // $user->username = $this->getRequest()->getParam('username');
            $user->name = $this->getRequest()->getParam('name');
            $user->suspended = $this->getRequest()->getParam('suspended')?1:0;
            $user->level = $this->getRequest()->getParam('level');
            $pass = $this->getRequest()->getParam('password');
            if ($pass) {
                $user->password = crypt($pass);
            }
            $user->save();
            $this->_redirect($this->view->url(array('action' => 'index', 'user_id' => null)));
        } else {
            $this->view->user = $user;
        }
    }
	
    public function wsconfigAction() {
		$sess = new Zend_Session_Namespace('scrapyard');
		$dbconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/sy_admin_properties.ini', APPLICATION_ENV);
		$dbAdmin= Zend_Db::factory( $dbconfig->db->admindb->adapter, $dbconfig->db->admindb->params->toArray() );			
		$dbAdmin->setFetchMode(Zend_Db::FETCH_OBJ);
    // Returns $result is a single object ... not an array of objects
		$sql = 'SELECT id,facilityRegNumber, webServiceID, webServicePW, state FROM sy_admin_clients WHERE applicationtag = ?';
		$client = $dbAdmin->fetchRow($sql,$sess->applicationTag);	
		if (!$client){
			$this->_logger->postIt('SY Administration Table did not return row for application tag<' . $sess->applicationTag. '>', Zend_Log::INFO);
		} 					
        if ($this->getRequest()->isPost()) {		
			$dbconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/sy_admin_properties.ini', APPLICATION_ENV);
			$dbAdmin= Zend_Db::factory( $dbconfig->db->admindb->adapter, $dbconfig->db->admindb->params->toArray() );			
            $table = 'sy_admin_clients';
		  	$data = array(
				'facilityRegNumber' =>  $this->getRequest()->getParam('facilityRegNumber'),
				'webServiceID'  	=>  $this->getRequest()->getParam('webServiceID'),
				'webServicePW'   	=>  $this->getRequest()->getParam('webServicePW') 
			);
			$dbAdmin= Zend_Db::factory( $dbconfig->db->admindb->adapter, $dbconfig->db->admindb->params->toArray() );			
            $row = $dbAdmin->quoteInto('id =?', $this->getRequest()->getParam('clientrowid'));
            $rowsAffected = $dbAdmin->update($table, $data, $row);
			if ( $rowsAffected > 0 )
				$this->view->message = 'Data has been updated';
			
			$dbAdmin->setFetchMode(Zend_Db::FETCH_OBJ);
			$sql = 'SELECT id,facilityRegNumber, webServiceID, webServicePW, state FROM sy_admin_clients WHERE applicationtag = ?';
			$client = $dbAdmin->fetchRow($sql,$sess->applicationTag);	
            $this->view->client = $client;
			$this->view->rowid = $client->id;
        } else {
            $this->view->client = $client;
			$this->view->rowid = $client->id;
			$this->view->message = '';
        }
    } 
 
}
 ?>
