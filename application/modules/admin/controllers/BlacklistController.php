<?php

class Admin_BlacklistController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function updateAction()
    {
        // show form to update blacklist
        $config = new SY_Model_DbTable_Config();
        $this->view->last_update = $config->
                select(true)
                ->where('name = ?','update_dnb')
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('value')
                ->query()
                ->fetchColumn();
    }
    
    public function ajaxDatatableAction() {
        
        $dataTables = new SY_Model_DataTables();
        $aColumns = array( 'dl_state', 'dl_number', 'first_name', 'last_name', 'middle_name', 'date_of_birth', 'sex', 'conviction_case', 'conviction_date' );
        $output = $dataTables->ajaxInteface(new SY_Model_DbTable_Customers_Blacklist(), $this->getRequest()->getParams(), $aColumns);
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        echo $output;
        return;
    }
    
    public function updatelistAction()
    {
        // do the actual updating
        if(!isset($_FILES) || !isset($_FILES['blacklist']))
        {
            //SY_Plugin_Log::log(print_r($_FILES, true));
            $this->_helper->flashMessenger->addMessage(array('status' => 'error', 'message' => "Internal Error, logged. Please contact developers."));
            $this->_redirect('/admin/blacklist/update');
        }
        // move the upload
        move_uploaded_file($_FILES['blacklist']['tmp_name'], APPLICATION_PATH . '/scripts/tmp.csv');
        // test the load
        $load = fopen(APPLICATION_PATH . '/scripts/tmp.csv', 'r');
            $header = fgetcsv($load);
            $header = array_flip($header);
            // Check for headers
            if (!isset($header['dl_state']) ||
                !isset($header['dl_number']) ||
                !isset($header['first_name']) ||
                !isset($header['last_name']) || 
                !isset($header['middle_initial']) ||
                !isset($header['last_name']) ||
                !isset($header['dob']) ||
                !isset($header['conviction_case_number']) ||
                !isset($header['conviction_text']) ||
                !isset($header['conviction_date']))
            {
                //$flash = $this->_helper->getHelper('flashMessenger');
                $this->_helper->flashMessenger->addMessage(array('status' => 'error', 'message' => "Invalid CSV"));
                //SY_Plugin_Log::log('BAD UPDATE');
                $this->_redirect('/admin/blacklist/update');
            }
            
            // do it
            $bl = new SY_Model_DbTable_Customers_Blacklist();
            $bl->updateBLviaCSV($load);
            $config = new SY_Model_DbTable_Config();
            $config->update(array('value' => date('m/d/Y')),'name = "update_dnb"');
            $flash = $this->_helper->getHelper('flashMessenger');
            $this->_helper->flashMessenger->addMessage(array('status' => 'success', 'message' => "Blacklist Updated"));
            $this->_redirect('/admin/blacklist/update');
    }
    
}

