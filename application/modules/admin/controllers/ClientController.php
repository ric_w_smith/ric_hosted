<?php

class Admin_ClientController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function searchAction() {
        // list all of the clients in the db
        $client = new SY_Model_Customer();
        $clients = $client->findAll();
        $this->view->clients = $clients;
        $formtype = $this->_request->getParam('formtype');
        if(isset($formtype))
        {
            $this->view->formtype = $formtype;
	}
	$co = new SY_Model_Corporate();
	$this->view->corps = $co->findAll();
    }

    public function ticketsAction()
    {
	$id = $this->_request->getParam('id', null);
	$tag = $this->_request->getParam('tag', null);
	$corp = $this->_request->getParam('corp', false);
	if(isset($id)) {
		if(!preg_match("/^[0-9]+$/i", $id))
		{
				$this->_helper->FlashMessenger->addMessage(array('message' => "Invalid customer id.", 'status' => 'error'));	
			//SY_Plugin_Log::log('ticket search for invalid customer id');
			$this->_redirect('/admin/client/search'); // redirect for bad data
		}
	
    $ti = new SY_Model_Ticket();
	$this->view->tickets = $ti->getCustomerClosed($id, $corp);
	} 
	if(isset($tag)) {
		$ti = new SY_Model_Ticket();
		$this->view->tickets = $ti->getCustomerClosedTag($tag, $corp);
    }
    $this->view->corp = $corp;
	}
    public function resultsAction() {
        $params = $this->_request->getParams();
        switch($params['formtype'])
        {
            case 'view':
                // update the passed-in client
                $cl = new SY_Model_Customer();
                $cl->id = $params['id'];
                $client = $cl->findOne();
                foreach(get_class_vars('SY_Model_Customer') as $key => $v)
                {
                    if($key !== '_db')
                    $client->$key = $params[$key];
                }
                if(isset($_FILES['picture_upload']['name']) && $_FILES['picture_upload']['name'] !== '')
                {
                    $client->handlePictureUpload($_FILES['picture_upload']);
                }
                $client->save();
                $params['formtype'] = 'edited';
                break;
                
            default:
                break;
        }
        
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_redirector->gotoSimple('search','client','admin', array('formtype' => $params['formtype']));
        
    }
    
    public function viewAction() {
        // get the id of the client to view
        $id = $this->_request->getParam('id');
        if(isset($id) && is_numeric($id) && 0 !== $id)
        {
            // sanity check
            $cl = new SY_Model_Customer();
            $cl->id = $id;
            $this->view->customer = $cl->findOne();
        }
    }
    
    public function deleteAction()
    {
        // get the id of the client to delete
        $id = $this->_request->getParam('id');
        if(isset($id) && is_numeric($id) && 0 !== $id)
        {
            // sanity check
            $cl = new SY_Model_Customer();
            $cl->id = $id;
            $client = $cl->findOne();
            $client->delete();
            $client->id = 0;
        }
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_redirector->gotoSimple('search','client','admin', array('formtype' => 'deleted'));
    }

    public function listCorporateAction()
    {
	$co = new SY_Model_Corporate();
	$this->view->clients = $co->findAll();
    }

    public function editCorporateAction()
    {
        $id = $this->_request->getParam('id', -1);
	if(!preg_match("/^[0-9]+$/i", $id)) { $id = -1; }

	$co = new SY_Model_Corporate();
	$co->id = $id;
	$client = $co->findOne();
	if (!$client) { $client = new SY_Model_Corporate(); }

	$cl = new SY_Model_Customer();
	$cl->corporate_affiliation = $id;
	$reps = $cl->findAll();

	$all = new SY_Model_Customer();

	$this->view->test = $id;
	$this->view->customer = $client;
	$this->view->reps = $reps;
	$this->view->all = $all->findAll();
    }

    public function insertCorporateAction()
    {
	$id = $this->_request->getParam('id', 0);
	$name = $this->_request->getParam('name', '');
	$addr1 = $this->_request->getParam('address_1', '');
	$addr2 = $this->_request->getParam('address_2', '');
	$city = $this->_request->getParam('city', '');
	$state = $this->_request->getParam('state', '');
	$zip = $this->_request->getParam('zip_code', '');

	$co = new SY_Model_Corporate();
	$co->id = $id;
	$co->name = $name;
	$co->address_1 = $addr1;
	$co->address_2 = $addr2;
	$co->city = $city;
	$co->state = $state;
	$co->zip_code = $zip;
	$co->save();

	$this->_redirect('/admin/client/search');
    }

    public function addRepAction()
    {
	$rep = $this->_request->getParam('id', 0);
	$customer = $this->_request->getParam('client', 0);

	$cl = new SY_Model_Customer();
	$cl->id = $rep;
	$rep = $cl->findOne();
	$rep->corporate_affiliation = $customer;
	$rep->save();

	$this->_redirect('/admin/client/edit-corporate/id/' . $customer);
    }

    public function removeRepAction()
    {
	$rep = $this->_request->getParam('id', 0);
	$customer = $this->_request->getParam('client', 0);

	$cl = new SY_Model_Customer();
	$cl->id = $rep;
	$rep = $cl->findOne();
	$rep->corporate_affiliation = 0;
	$rep->save();

	$this->_redirect('/admin/client/edit-corporate/id/' . $customer);
    }
}

