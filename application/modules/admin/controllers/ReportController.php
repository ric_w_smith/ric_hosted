<?php

class Admin_ReportController extends App_Controller_Action
{

	protected $logger;

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
		Zend_Loader::loadClass('WebserviceClient', APPLICATION_PATH."/models");
		$this->_logger = new ScrapYardLogger();
    }

    public function indexAction()
    {
        // action body
    }

    public function detailedAction()
    {
        if ($this->getRequest()->getParam('beginDate',null)) {
            $beginDate = strtotime($this->getRequest()->getParam('beginDate',null));
        } else {
            $beginDate = strtotime(date('Y-m-d'));
        }
        $this->view->start_date = $beginDate;
        if ($this->getRequest()->getParam('endDate',null)) {
            $endDate = strtotime($this->getRequest()->getParam('endDate',null));
        } else {
            $endDate = $beginDate;
        }
        $this->view->end_date = $endDate;
        // see if there's a scale selected
        $params = $this->_request->getParams();
        $ticket_model = new SY_Model_Ticket();
        if(isset($params['scale_id']) && '' !== $params['scale_id'])
        {
            $this->view->metaltotals = $ticket_model->getMetalTotals($params['scale_id'], $beginDate, $endDate);
            $this->view->scale_id = $params['scale_id'];
        }
        else
        {
            $this->view->metaltotals = $ticket_model->getMetalTotals(null,$beginDate,$endDate);
        }
    }
	
	public function submitticketsAction() {		

        if ($this->getRequest()->getParam('beginDate',null)) {
            $beginDate = $this->getRequest()->getParam('beginDate'); 
		} else {
            $beginDate = date('Y-m-d');
        }
        if ($this->getRequest()->getParam('endDate',null)) {
            $endDate = $this->getRequest()->getParam('endDate');
			
		} else {
            $endDate = date('Y-m-d');
		}	
        $this->view->begin_Date = $beginDate;
        $this->view->end_Date = $endDate;
		$webService = new WebserviceClient();
		$this->view->ticketInfo = $webService->getTicketsToUpload($beginDate, $endDate,"all");	
	}
		
	public function dailyticketAction()
    {
        if ($this->getRequest()->getParam('beginDate',null)) {
            $beginDate = strtotime($this->getRequest()->getParam('beginDate',null));
        } else {
            $beginDate = strtotime(date('Y-m-d'));
        }
        $this->view->start_date = $beginDate;
        if ($this->getRequest()->getParam('endDate',null)) {
            $endDate = strtotime($this->getRequest()->getParam('endDate',null));
        } else {
            $endDate = $beginDate;
        }
        $this->view->end_date = $endDate;
        // see if there's a scale selected
       // $params = $this->_request->getParams();
        $ticket_model = new SY_Model_Ticket();
	//build the view
	$scale_id = $this->getRequest()->getParam('scale_id',null);
	$login_id = $this->getRequest()->getParam('login_id',null);
	if ($scale_id)
	{
	    $this->view->scale_id = $scale_id;
	}
	if ($login_id)
	{
	    $this->view->login_id = $login_id;
	}
	
            $this->view->ticketInfo = $ticket_model->getAllClosed($beginDate, $endDate, $scale_id, $login_id);
       
    }
	
    
    public function basicAction()
    {
        if ($this->getRequest()->getParam('txtDate',null)) {
            $date = strtotime($this->getRequest()->getParam('txtDate',null));
        } else {
            $date = strtotime(date('Y-m-d'));
        }
        $this->view->date = $date;
        // see if there's a scale selected
        $params = $this->_request->getParams();
        $ticket_model = new SY_Model_Ticket();
        
        $ac = new SY_Model_Account();
        $ac->date = date('Y-m-d',$date);
        $acc = $ac->findOne();
        if (!$acc) {
            $this->view->acc = $ac;
        } else {
            $this->view->acc = $acc;
        }
        
        if(isset($params['scale_id']) && '' !== $params['scale_id'])
        {
            $this->view->total_open = $ticket_model->getTotalOpen($params['scale_id'], $date);
            $this->view->total_closed = $ticket_model->getTotalClosed($params['scale_id'], $date);
            $this->view->scale_id = $params['scale_id'];
        }
        else
        {
            $this->view->total_open = $ticket_model->getTotalOpen(null, $date);
            $this->view->total_closed = $ticket_model->getTotalClosed(null, $date);
        }
    }
}

