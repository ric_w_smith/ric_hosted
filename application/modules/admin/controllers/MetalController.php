<?php

class Admin_MetalController extends App_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function pricesAction()
    {
        // get all the metal prices
        $met = new SY_Model_Metal();
        $this->view->metals = $met->findAll(); // defaults to all active metals
        if($this->_request->getParam('updated') == '1')
        {
            $this->view->updated = true;
        }
    }
    
    public function updatepricesAction()
    {
        // get the new info
        $new = $this->_request->getParams();
        // get all the current metal info
        $met = new SY_Model_Metal();
        $old = $met->findAll();
        foreach($old as $thisMetal)
        {
            $curId = $thisMetal->id;
            // check the values
            if($new['active'][$curId] != $thisMetal->active ||
                    $new['form_order'][$curId]-1 != $thisMetal->form_order ||
                    $new['price_per_unit'][$curId] != $thisMetal->price_per_unit ||
                    $new['name'][$curId] != $thisMetal->name)
            {
                $thisMetal->active = $new['active'][$curId];
                $thisMetal->form_order = $new['form_order'][$curId] - 1;
                if($new['name'][$curId] != $thisMetal->name) $thisMetal->name = $new['name'][$curId];
                $thisMetal->price_per_unit = $new['price_per_unit'][$curId];
                $thisMetal->save();
            }
        }
        // see if there's any new ones
        if(isset($new['activenew']))
        {
            // we have new stuff
            // push the params into easier-to-read arrays
            $newMetals = array();
            $newMetals['active'] = array();
            foreach($new['activenew'] as $thisActive)
            {
                $newMetals['active'][] = $thisActive;
            }
            foreach($new['form_ordernew'] as $thisForm)
            {
                $newMetals['form_order'][] = $thisForm;
            }
            foreach($new['namenew'] as $thisName)
            {
                $newMetals['name'][] = $thisName;
            }
            foreach($new['price_per_unitnew'] as $thisPPU)
            {
                $newMetals['price_per_unit'][] = $thisPPU;
            }
            // use the currently-existing Metal object
            $newMetCount = count($newMetals['active']);
            for($i = 0; $i < $newMetCount; $i++)
            {
                // iterate through the new metals, adding them
                $newMet = new SY_Model_Metal();
                $newMet->active = $newMetals['active'][$i];
                $newMet->form_order = $newMetals['form_order'][$i] - 1;
                $newMet->name = $newMetals['name'][$i];
                $newMet->price_per_unit = $newMetals['price_per_unit'][$i];
                $newMet->save();
            }
        }
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_redirector->gotoSimple('prices','metal','admin', array('updated' => '1'));
    }
}

