<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initValidateCompany()	
    {		
		$sess = new Zend_Session_Namespace('scrapyard');
		$sess->applicationTag = 'mge';	

/* When hosting will use this code to determine the company id, here we are hard coding mark gray, by the URL */
//	    if (!isset($sess->applicationTag) or ($sess->applicationTag != $_SERVER['SERVER_NAME'])                         ) { 
//			$pieces = explode(".", $_SERVER['SERVER_NAME']);
//		    if (count($pieces) > 0) {
//				$sess->applicationTag  = $pieces[0];
//			} 
//      } 
/*  this code can be used when we want to verify that the client account is still active, else block them post error
        try {
			$dbconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/sy_admin_properties.ini', APPLICATION_ENV);
			$dbAdmin= Zend_Db::factory( $dbconfig->db->admindb->adapter, $dbconfig->db->admindb->params->toArray() );			
			$dbAdmin->setFetchMode(Zend_Db::FETCH_OBJ);
          // Returns $result is a single object ... not an array of objects
			$sql = 'SELECT status FROM sy_admin_clients WHERE applicationtag = ?';
			$result = $dbAdmin->fetchRow($sql, $sess->applicationTag);	
			$sess->clientStatus = $result->status;
			$dbdbAdmin = null;
			} catch (Zend_Config_Exception $e) {
				throw new Exception($e->toString());
        }		
 */
	}

 	protected function _initLocalConfig()
    {
        $globalConfig = new Zend_Config($this->getOptions(), true);
		$sess = new Zend_Session_Namespace('scrapyard');
        try {
		$sess = new Zend_Session_Namespace('scrapyard');
		$Config = APPLICATION_PATH . '/configs/' . $sess->applicationTag . '_properties.ini';
			$localConfig = new Zend_Config_Ini($Config, APPLICATION_ENV);				
            $globalConfig->merge($localConfig);
            $this->setOptions($globalConfig->toArray());
        } catch (Zend_Config_Exception $e) {
            throw new Exception('File: ' . $Config . ' not found. Create it, it can be empty. A sample file is provided in /configs/local.ini.dist');
        }

	}

    protected function _initZZDatabaseUpdate() {
        // This is so init scripts and stuff work still.
        if (!defined('APPLICATION_DO_NOT_UPGRADE')) {
            $options = $this->getOptions(APPLICATION_ENV);
            $dbconfig = $options['resources']['db'];
            $db = Zend_Db::factory($dbconfig['adapter'], $dbconfig['params']);
            $update = new SY_Model_DbTable_Config(array('db' => $db));
            $update->doUpdate();
        }
    }
}

?>