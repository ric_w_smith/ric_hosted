<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Log
 *
 * @author ostin
 */
class SY_Plugin_Log {
    
    
    public static function log($string)
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $options = $bootstrap->getOptions();
		$filename = $options['logpath'] . "Scrapyard.log";
        $writer = new Zend_Log_Writer_Stream($filename);		
        $log = new Zend_Log($writer);
        $log->info($string);
    }
}

?>
