<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author wlightning
 */
class SY_Plugin_Auth extends Zend_Controller_Plugin_Abstract {
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        // check if we're in a model that requires auth.
        if ('auth' == $controller && 'default' == $module)
           return;
        

        if (!(Zend_Auth::getInstance()->hasIdentity() && Zend_Auth::getInstance()->getIdentity() instanceOf SY_Model_AuthIdentity)) {
            $this->getResponse()->setRedirect('/auth');
        } else {
            $ns = new Zend_Session_Namespace(Zend_Auth::getInstance()->getStorage()->getNamespace());
            // set expire time
            $ns->setExpirationSeconds(60 * 60);
            /* Useful if we ever restrict permissions... */
            $identity = Zend_Auth::getInstance()->getIdentity();
            // if ($request->getModuleName() == 'office' && !$identity->minLevel('office')) {
            //    $this->getResponse()->setRedirect('/');
            //} elseif ($request->getModuleName() == 'yard' && !$identity->minLevel('yard')) {
            //    $this->getResponse()->setRedirect('/');
            // } else
            if ($request->getModuleName() == 'admin' && !$identity->minLevel('admin')) {
                $this->getResponse()->setRedirect('/');
            }
        }

        /* Useful for requiring changes of passwords.
        elseif (Zend_Auth::getInstance()->getIdentity()->needChangePass) {
            $this->getResponse()->setRedirect('/auth/chgpass');
        } */
    }
}

?>
