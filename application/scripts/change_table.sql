CREATE TABLE sy_customers_corporate (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    date_created DATETIME,
    date_modified DATETIME,
    name varchar(256),
    address_1 varchar(64),
    address_2 varchar(64),
    city varchar(64),
    state varchar(2),
    zip_code varchar(10),
    PRIMARY KEY (id)
);

ALTER TABLE sy_customers ADD corporate_affiliation INT UNSIGNED NOT NULL AFTER address_2;
ALTER TABLE sy_tickets ADD corporate_ticket SMALLINT(1) NOT NULL DEFAULT 0 AFTER visittype;
ALTER TABLE sy_tickets ADD tag_state CHAR(2) NOT NULL AFTER corporate_ticket
