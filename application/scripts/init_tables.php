<?php

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/..'));
set_include_path(implode(PATH_SEPARATOR, array(APPLICATION_PATH . '/../library', get_include_path(),)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

defined('APPLICATION_ENV') || define('APPLICATION_ENV', 'development');
define('APPLICATION_DO_NOT_UPGRADE', '1');

$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
$application->bootstrap();
$bootstrap = $application->getBootstrap();
$dbAdapter = $bootstrap->getResource('db');

try {
$schema = file_get_contents(dirname(__FILE__) . '/seed_database.sql');
$schema = str_replace('__KORE_DEFAULT_PASS__',$dbAdapter->quote(crypt('Kore$1234')), $schema);
$schema = str_replace('__DEFAULT_PASS__',$dbAdapter->quote(crypt('scrap')), $schema);
$dbAdapter->getConnection()->exec($schema);
$blacklist = new SY_Model_DbTable_Customers_Blacklist();
$blacklist_fpointer = fopen(dirname(__FILE__) . '/blocklist.csv','r');
$blacklist->updateBLviaCSV($blacklist_fpointer);
fclose($blacklist_fpointer);
}
catch (Exception $err) { echo 'Error: ' . $err->getMessage(); }
