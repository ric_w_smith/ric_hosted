CREATE TABLE sy_metals (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    name varchar (64),
    price_per_unit DECIMAL(8,2) DEFAULT 0.00,
    date_modified DATETIME,
    active SMALLINT(1) NOT NULL DEFAULT 1,
    form_order INT UNSIGNED NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sy_metals_history (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    metal_id INT UNSIGNED NOT NULL,
    date DATETIME,
    price_per_unit DECIMAL(8,2) DEFAULT 0.00,
    active SMALLINT(1) NOT NULL DEFAULT 1,
    PRIMARY KEY(id)
);

CREATE TABLE sy_customers (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    account_state INT,
    date_created DATETIME,
    date_modified DATETIME,
    state VARCHAR(2),
    zip_code VARCHAR(10),
    city VARCHAR(32),
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    middle_name VARCHAR(32),
    date_of_birth DATE,
    drivers_license_number VARCHAR(32),
    address_1 VARCHAR(64),
    address_2 VARCHAR(64),
    `picture_mimetype` varchar(50),
    picture MEDIUMBLOB,
    PRIMARY KEY(id)
);

CREATE TABLE sy_scales (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS sy_accounts (
  id int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  balance decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (id),
  KEY `date` (`date`)
);


CREATE TABLE sy_customers_blacklist (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    dl_state VARCHAR(2),
    dl_number VARCHAR(15),
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    middle_name VARCHAR(32),
    date_of_birth DATE,
    sex VARCHAR(32),
    conviction_case VARCHAR(32),
    conviction_text VARCHAR(64),
    conviction_date DATE,
    PRIMARY KEY(id)
);

CREATE TABLE sy_tickets (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    customer_id INT UNSIGNED NOT NULL,
    scale_id INT UNSIGNED NOT NULL,
    ticket_state INT UNSIGNED NOT NULL,
    date_created DATETIME,
    sig_string TEXT NOT NULL,
    PRIMARY KEY(id)
);


CREATE TABLE sy_purchases (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    ticket_id INT UNSIGNED NOT NULL,
    metal_id INT UNSIGNED NOT NULL,
    gross DECIMAL(8,2),
    tare DECIMAL(8,2),
    price_per_unit DECIMAL(8,2),
    PRIMARY KEY(id)
);

CREATE TABLE sy_logins (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(250) NULL DEFAULT NULL,
    name VARCHAR(100) NOT NULL,
    suspended TINYINT(1) NOT NULL DEFAULT 0,
    resetpass TINYINT(1) NOT NULL DEFAULT 0,
    level ENUM('yard','office','admin') NOT NULL DEFAULT 'admin',
    PRIMARY KEY(id)
);

CREATE TABLE sy_config (
  id int UNSIGNED AUTO_INCREMENT NOT NULL,
  name varchar(50) NOT NULL DEFAULT '',
  value varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (id),
  UNIQUE KEY name (name)
);
