<?php

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/..'));
set_include_path(implode(PATH_SEPARATOR, array(APPLICATION_PATH . '/../library', get_include_path(),)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

defined('APPLICATION_ENV') || define('APPLICATION_ENV', 'development');

$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
$application->bootstrap();
$bootstrap = $application->getBootstrap();
$dbAdapter = $bootstrap->getResource('db');

try {
$schema = file_get_contents(dirname(__FILE__) . '/change_table.sql');
$dbAdapter->getConnection()->exec($schema);
}
catch (Exception $err) { echo 'Error: ' . $err->getMessage(); }
