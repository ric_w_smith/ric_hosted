// MESSAGE BOX FADING SCRIPTS ---------------------------------------------------------------------

$(document).ready(function() {
        $(".close-yellow").click(function () {
                $("#message-yellow").fadeOut("slow");
        });
        $(".close-red").click(function () {
                $("#message-red").fadeOut("slow");
        });
        $(".close-blue").click(function () {
                $("#message-blue").fadeOut("slow");
        });
        $(".close-green").click(function () {
                $("#message-green").fadeOut("slow");
        });
});


// START LOGIN PAGE SHOW HIDE BETWEEN LOGIN AND FORGOT PASSWORD BOXES--------------------------------------

$(document).ready(function () {
	$(".forgot-pwd").click(function () {
	$("#loginbox").hide();
	$("#forgotbox").show();
	return false;
	});

});

$(document).ready(function () {
	$(".back-login").click(function () {
	$("#loginbox").show();
	$("#forgotbox").hide();
	return false;
	});
});

// END ----------------------------- 2